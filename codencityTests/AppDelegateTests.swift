//
//  AppDelegateTests.swift
//  codencityTests
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

@testable import codencityDev
import XCTest

class AppDelegateTests: XCTestCase {

    var sut: AppDelegate?

    override func setUp() {

        super.setUp()
        sut = AppDelegate()
    }

    override func tearDown() {

        sut = nil
        super.tearDown()
    }

    func testDidFinishLaunchingWithOptionsReturnsTrue() {

        guard let check = sut?.application(UIApplication.shared, didFinishLaunchingWithOptions: nil) else { XCTFail(">>> no sut"); return }
        XCTAssertTrue(check, "ApplicationDidFinishLaunching should return True")
    }

    func testWindowExists() {

        _ = sut?.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        XCTAssertNotNil(sut?.window, "Window should be created at app start")
    }

    func testRootControllerExists() {

        _ = sut?.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        XCTAssertNotNil(sut?.window?.rootViewController, "RootViewcontroller should be created and set")
    }

    func testRootViewControllerValidates() {

        _ = sut?.application(UIApplication.shared, didFinishLaunchingWithOptions: nil)
        XCTAssertTrue(sut?.window?.rootViewController is UINavigationController, "RootViewController should be NavigationController")
    }
}
