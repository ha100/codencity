//
//  APITests.swift
//  codencityTests
//
//  Created by tom Hastik on 19/04/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

@testable import codencityDev
import XCTest

final class APITests: XCTestCase {

    func fromMockFile(_ named: String) -> Data? {

        guard let path = Bundle(for: type(of: self)).path(forResource: named, ofType: "json"),
            let url = URL(string: "file://" + path) else { return nil }

        do {

            return try Data(contentsOf: url)

        } catch let error {
            print(">>> \(error)")
        }

        return nil
    }

    override func setUp() {

        super.setUp()
    }

    override func tearDown() {

        super.tearDown()
    }

    func testGetReposReturnsReposForValidData() {

        let expectation = self.expectation(description: "Promise")

        let mockData = fromMockFile("getRepos")
        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 200,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (mockData, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        API.getRepos(session, for: settings.githubUser)
            .observe { repos in

                guard !repos.isEmpty else {

                    XCTFail("array should not be nil when loaded from mocked json data")
                    return
                }

                expectation.fulfill()
            }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testGetReposWorksForEmptyData() {

        let expectation = self.expectation(description: "Promise")

        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 200,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (nil, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        API.getRepos(session, for: settings.githubUser)
            .observe { repos in

                guard repos.isEmpty else {

                    XCTFail("array should be empty for nil response data")
                    return
                }

                expectation.fulfill()
            }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testGetReposWorksForAllStatusCodes() {

        let expectation = self.expectation(description: "Promise")

        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 300,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (nil, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        API.getRepos(session, for: settings.githubUser)
            .observe { repos in

            guard repos.isEmpty else { XCTFail("array should be empty for invalid status code"); return }

            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testGetReposWorksForNoResponse() {

        let expectation = self.expectation(description: "Promise")

        MockSession.mockResponse = (nil, urlResponse: nil, error: nil)
        let session = MockSession.shared

        API.getRepos(session, for: settings.githubUser)
            .observe { repos in

            guard repos.isEmpty else { XCTFail("array should be empty for no server response"); return }

            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testGetReposRetryWorks() {

        let expectation = self.expectation(description: "Promise")

        MockSession.mockResponse = (nil, urlResponse: nil, error: nil)
        let session = MockSession.shared

        API.getRepos(session, for: settings.githubUser)
            .observe { repos in

            guard repos.isEmpty else { XCTFail("array should be empty for no server response"); return }

            expectation.fulfill()
        }

        waitForExpectations(timeout: 10, handler: nil)
    }

    func testGetUserReturnsDetailsForValidData() {

        let expectation = self.expectation(description: "Promise")

        let mockData = fromMockFile("getUser")
        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 200,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (mockData, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        API.getUser(session, named: settings.githubUser)
            .observe { user in

            XCTAssertNotNil(user, "should return valid user for mocked json data")
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testGetUserDetectsEmptyData() {

        let expectation = self.expectation(description: "Promise")

        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 200,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (nil, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        API.getUser(session, named: settings.githubUser)
            .observe { _ in

                XCTFail("non existent user data should be detected with networkEmptyData")
            }
            .catch { error in

                XCTAssertEqual(error, CodencityError.networkEmptyData, "non existent user data should be detected with networkEmptyData")
                expectation.fulfill()
            }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testGetUserWorksForAllStatusCodes() {

        let expectation = self.expectation(description: "Promise")

        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 300,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (nil, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        API.getUser(session, named: settings.githubUser)
            .observe { _ in

                XCTFail("should've gone the catch route")
        }
            .catch { error in

                XCTAssertEqual(error,
                               .networkError(message: "N/A", code: 300),
                               "network error should detect 300 status code")
                expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testGetUserWorksForNoResponse() {

        let expectation = self.expectation(description: "Promise")

        MockSession.mockResponse = (nil, urlResponse: nil, error: nil)
        let session = MockSession.shared

        API.getUser(session, named: settings.githubUser)
            .observe { _ in

                XCTFail("should've gone the catch route")
        }
            .catch { error in

                XCTAssertEqual(error,
                               .unknownError(msg: "emptyResponse"),
                               "user should be nil for no response")
                expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)
    }
}
