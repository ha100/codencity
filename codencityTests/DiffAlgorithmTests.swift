//
//  DiffAlgorithmTests.swift
//  codencityTests
//
//  Created by tom Hastik on 12/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//
//  Credits: Matias Cudich https://github.com/mcudich/HeckelDiff

@testable import codencityDev
import XCTest

final class DiffTests: XCTestCase {

    func testEmptyArrays() {

        let oldArray: Array<Int> = []
        let newArray: Array<Int> = []
        let result = diff(oldArray, newArray)

        XCTAssertEqual(0, result.count)
    }

    func testDiffingFromEmptyArray() {

        let oldArray: Array<Int> = []
        let newArray = [1]
        let result = diff(oldArray, newArray)

        XCTAssertEqual(.insert(0), result[0])
        XCTAssertEqual(1, result.count)
    }

    func testDiffingToEmptyArray() {

        let oldArray = [1]
        let newArray: Array<Int> = []
        let result = diff(oldArray, newArray)

        XCTAssertEqual(.delete(0), result[0])
        XCTAssertEqual(1, result.count)
    }

    func testSwapHasMoves() {

        let oldArray = [1, 2, 3]
        let newArray = [2, 3, 1]
        let result = diff(oldArray, newArray)

        XCTAssertEqual([.move(1, 0), .move(2, 1), .move(0, 2)], result)
    }

    func testSwapHasMovesWithOrder() {

        let oldArray = [1, 2, 3]
        let newArray = [2, 3, 1]
        let result = orderedDiff(oldArray, newArray)

        XCTAssertEqual([.delete(2), .delete(1), .delete(0), .insert(0), .insert(1), .insert(2)], result)
    }

    func testMovingTogether() {

        let oldArray = [1, 2, 3, 3, 4]
        let newArray = [2, 3, 1, 3, 4]
        let result = diff(oldArray, newArray)

        XCTAssertEqual([.move(1, 0), .move(2, 1), .move(0, 2)], result)
    }

    func testMovingTogetherWithOrder() {

        let oldArray = [1, 2, 3, 3, 4]
        let newArray = [2, 3, 1, 3, 4]
        let result = orderedDiff(oldArray, newArray)

        XCTAssertEqual([.delete(2), .delete(1), .delete(0), .insert(0), .insert(1), .insert(2)], result)
    }

    func testSwappedValuesHaveMoves() {

        let oldArray = [1, 2, 3, 4]
        let newArray = [2, 4, 5, 3]
        let result = diff(oldArray, newArray)

        XCTAssertEqual([.delete(0), .move(3, 1), .insert(2), .move(2, 3)], result)
    }

    func testSwappedValuesHaveMovesWithOrder() {

        let oldArray = [1, 2, 3, 4]
        let newArray = [2, 4, 5, 3]
        let result = orderedDiff(oldArray, newArray)

        XCTAssertEqual([.delete(3), .delete(2), .delete(0), .insert(1), .insert(2), .insert(3)], result)
    }

    func testUpdates() {

        let oldArray = [ DiffMock(value: 0, eValue: 0),
                         DiffMock(value: 1, eValue: 1),
                         DiffMock(value: 2, eValue: 2) ]

        let newArray = [ DiffMock(value: 0, eValue: 1),
                         DiffMock(value: 1, eValue: 2),
                         DiffMock(value: 2, eValue: 3) ]

        let result = diff(oldArray, newArray)

        XCTAssertEqual([.update(0), .update(1), .update(2)], result)
    }

    func testDeletionLeadingToInsertionDeletionMoves() {

        let oldArray = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        let newArray = [0, 2, 3, 4, 7, 6, 9, 5, 10]
        let result = diff(oldArray, newArray)

        XCTAssertEqual([.delete(1), .delete(8), .move(7, 4), .insert(6), .move(5, 7), .insert(8)], result)
    }

    func testDeletionLeadingToInsertionDeletionMovesWithOrder() {

        let oldArray = [0, 1, 2, 3, 4, 5, 6, 7, 8]
        let newArray = [0, 2, 3, 4, 7, 6, 9, 5, 10]
        let result = orderedDiff(oldArray, newArray)

        XCTAssertEqual([.delete(8), .delete(7), .delete(5), .delete(1), .insert(4), .insert(6), .insert(7), .insert(8)], result)
    }

    func testMovingWithEqualityChanges() {

        let oldArray = [ DiffMock(value: 0, eValue: 0),
                         DiffMock(value: 1, eValue: 1),
                         DiffMock(value: 2, eValue: 2) ]

        let newArray = [ DiffMock(value: 2, eValue: 3),
                         DiffMock(value: 1, eValue: 1),
                         DiffMock(value: 0, eValue: 0) ]

        let result = orderedDiff(oldArray, newArray)

        XCTAssertEqual([.delete(2), .delete(0), .insert(0), .insert(2), .update(0)], result)
    }

    func testDeletingEqualObjects() {

        let oldArray = [0, 0, 0, 0]
        let newArray = [0, 0]
        let result = diff(oldArray, newArray)

        XCTAssertEqual(2, result.count)
    }

    func testInsertingEqualObjects() {

        let oldArray = [0, 0]
        let newArray = [0, 0, 0, 0]
        let result = diff(oldArray, newArray)

        XCTAssertEqual(2, result.count)
    }

    func testInsertingWithOldArrayHavingMultipleCopies() {

        let oldArray = [NSObject(), NSObject(), NSObject(), 49, 33, "cat", "cat", 0, 14] as [AnyHashable]
        var newArray = oldArray
        newArray.insert("cat", at: 5)
        let result = diff(oldArray, newArray)

        XCTAssertEqual(1, result.count)
    }
}
