//
//  FutureTests.swift
//  codencityTests
//
//  Created by tom Hastik on 31/07/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

@testable import codencityDev
import XCTest

// swiftlint:disable file_length type_body_length

final class FutureTests: XCTestCase {

    // MARK: - Properties

    var sut: String?

    // MARK: - LifeCycle

    /// Put setup code here. This method is called before the invocation
    /// of each test method in the class.
    ///
    override func setUp() {

        super.setUp()
        sut = nil
    }

    /// Put teardown code here. This method is called after the invocation
    /// of each test method in the class.
    ///
    override func tearDown() {

        sut = nil
        super.tearDown()
    }

    func delay(_ seconds: Double, completion: @escaping () -> Void) {

        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }

    func defaultsLoad(_ passes: Bool = true, resolvesWith: String = "XXX") -> Future<String> {

        return Promise<String> { future in

            self.delay(0.1) {
                passes ? future.resolve(with: resolvesWith) : future.reject(with: .urlCreationError)
            }
        }
    }

    // MARK: - UnitTests

    func testStateValuePropertyWorksAsExpectedAfterResolution() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")

        let promise = Promise<String> { future in

                future.resolve(with: outcome)
            }
            .observe { _ in

                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.state.value, outcome)
    }

    func testStateValuePropertyWorksAsExpectedAfterRejection() {

        let expectation = self.expectation(description: "promise")

        let promise = Promise<String> { future in

                future.reject(with: .promiseRetryLimitReachedError)
            }
            .observe { _ in

                XCTFail("should not resolve the chain")
            }
            .catch { _ in

                expectation.fulfill()
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.state.value, nil)
    }

    func testNoMultipleRejection() {

        let expectation = self.expectation(description: "promise")

        let promise = Promise<String> { future in

                future.reject(with: .promiseRetryLimitReachedError)
                future.reject(with: .promiseExecutionTimedOut)
            }
            .observe { _ in

                XCTFail("should not resolve the chain")
            }
            .catch { _ in

                expectation.fulfill()
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.state.error, .promiseRetryLimitReachedError)
        XCTAssertEqual(promise.state.value, nil)
    }

    func testStateErrorPropertyWorksAsExpectedAfterResolution() {

        let expectation = self.expectation(description: "promise")

        let promise = Promise<String> { future in

                future.reject(with: .promiseRetryLimitReachedError)
            }
            .observe { _ in

                XCTFail("should not resolve the chain")
            }
            .catch { _ in

                expectation.fulfill()
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.state.error, .promiseRetryLimitReachedError)
    }

    func testStateErrorPropertyWorksAsExpectedAfterRejection() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")

        let promise = Promise<String> { future in

                future.resolve(with: outcome)
            }
            .observe { _ in

                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.state.error, nil)
    }

    func testStateIsPendingPropertyWorksAsExpectedAfterResolution() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")

        let promise = Promise<String> { future in

                future.resolve(with: outcome)
            }
            .observe { _ in

                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.state.isPending, false)
    }

    func testStateIsPendingPropertyWorksAsExpectedAfterRejection() {

        let expectation = self.expectation(description: "promise")

        let promise = Promise<String> { future in

                future.reject(with: .promiseRetryLimitReachedError)
            }
            .observe { _ in

                XCTFail("should not resolve the chain")
            }
            .catch { _ in

                expectation.fulfill()
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.state.isPending, false)
    }

    func testStateIsPendingPropertyWorksAsExpectedWhilePending() {

        let promise = Promise<String> { future in

            }
            .observe { _ in

                XCTFail("should not resolve the chain")
            }
            .catch { _ in
                XCTFail("should not fail the chain")
            }

        XCTAssertEqual(promise.state.isPending, true)
    }

    func testStateIsRejectedPropertyWorksAsExpectedAfterResolution() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")

        let promise = Promise<String> { future in

                future.resolve(with: outcome)
            }
            .observe { _ in

                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.state.isRejected, false)
    }

    func testStateIsRejectedPropertyWorksAsExpectedAfterRejection() {

        let expectation = self.expectation(description: "promise")

        let promise = Promise<String> { future in

                future.reject(with: .promiseRetryLimitReachedError)
            }
            .observe { _ in

                XCTFail("should not resolve the chain")
            }
            .catch { _ in

                expectation.fulfill()
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.state.isRejected, true)
    }

    func testStateIsRejectedPropertyWorksAsExpectedWhilePending() {

        let promise = Promise<String> { future in

            }
            .observe { _ in

                XCTFail("should not resolve the chain")
            }
            .catch { _ in

                XCTFail("should not fail the chain")
            }

        XCTAssertEqual(promise.state.isRejected, false)
    }

    func testInitClosureIsCalledImmediately() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")

        _ = Promise<String>(file: "") { future in

            self.sut = outcome
            expectation.fulfill()
            future.resolve(with: outcome)
        }

        waitForExpectations(timeout: 1, handler: nil)
        XCTAssertEqual(self.sut, outcome)
    }

    func testDeinitClosureIsCalled() {

        let mockReporter = MockReporter()

        _ = Promise<String>(reporter: mockReporter) { future in

                future.resolve(with: "test")
            }

        XCTAssertEqual(mockReporter.methodCalled, true)
    }

    func testVoidPromiseResolveClosureIsCalled() {

        let expectation = self.expectation(description: "promise")

        _ = Promise<Void> { future in

                future.resolve()

            }.observe { _ in

                expectation.fulfill()
            }
            .catch { _ in

                XCTFail("should not fail the chain")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
    }

    func testValueInitializerWorksAsExpected() {

        var promise: Promise<Void>?

        promise = Promise(value: ())

        XCTAssertNotNil(promise)
        XCTAssertEqual(promise?.state.isPending, false)
    }

    func testValueInitializerWorksAsExpectedForNilValue() {

        var promise: Promise<Void>?

        promise = Promise(value: nil)

        XCTAssertEqual(promise?.state.isPending, true)
    }

    func testPromiseNameIsSet() {

        let expectation = self.expectation(description: "promise")
        let filename = "Optional(\"FutureTests.swift\"): testPromiseNameIsSet()"

        let promise = Promise<Void> { future in

                future.resolve()

            }.observe { _ in

                expectation.fulfill()
            }
            .catch { _ in

                XCTFail("should not fail the chain")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(promise.name, filename)
        XCTAssertEqual(promise.description, filename)
        XCTAssertEqual(promise.debugDescription, filename)
    }

    func testResolutionHandlerCalledWhenPromiseResolvesSync() {

        let outcome = "foo"
        let mockReporter = MockReporter()
        let expectation = self.expectation(description: "promise")

        _ = Promise<String>(reporter: mockReporter) { future in

                future.resolve(with: outcome)
            }
            .observe { value in

                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, outcome)
        XCTAssertEqual(mockReporter.methodCalled, true)
    }

    func testResolutionHandlerCalledWhenPromiseResolvesAsync() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .observe { value in

                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, outcome)
    }

    func testPromiseSupportsManyResolutionHandlersSync() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0

        _ = Promise<String> { future in

                future.resolve(with: outcome)
            }
            .observe { _ in

                assertions += 1
            }
            .observe { value in

                assertions += 1
                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, outcome)
        XCTAssertEqual(assertions, 2)
    }

    func testPromiseSupportsManyResolutionHandlersAsync() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .observe { _ in

                assertions += 1
            }
            .observe { value in

                assertions += 1
                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, outcome)
        XCTAssertEqual(assertions, 2)
    }

    func testResolutionHandlersCanChain() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .chained { value in

                Promise<String> { future in

                    self.delay(0.1) {

                        assertions += 1
                        future.resolve(with: value + value)
                    }
                }
            }
            .observe { value in

                assertions += 1
                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, outcome + outcome)
        XCTAssertEqual(assertions, 2)
    }

    func testTransformationWorksInPromiseChain() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .transform { value -> String in

                assertions += 1
                return value + value
            }
            .observe { value in

                assertions += 1
                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, outcome + outcome)
        XCTAssertEqual(assertions, 2)
    }

    func testPromiseCatchesErrorInChain() {

        let expectation = self.expectation(description: "promise")
        var assertions = 0

        let string = "foo"
        let eerror: CodencityError = .urlCreationError
        var localSut: CodencityError?

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: string)
                }
            }
            .chained { _ -> Future<String> in

                assertions += 1
                return self.defaultsLoad(false)
            }
            .observe { _ in

                XCTFail("should not resolve the chain")
            }
            .catch { error in

                assertions += 1
                localSut = error
                expectation.fulfill()
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(localSut, eerror)
        XCTAssertEqual(assertions, 2)
    }

    func testPromiseIgnoresCatchBlockWhenNoErroInChain() {

        let transformed = "XXX"
        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0

        _ = Promise<String> { future in

            self.delay(0.1) {
                future.resolve(with: outcome)
            }
        }
        .chained { _ -> Future<String> in

            assertions += 1
            return self.defaultsLoad(true, resolvesWith: transformed)
        }
        .observe { value in

            assertions += 1
            self.sut = value
            expectation.fulfill()
        }
        .catch { error in

            XCTFail("should not fail to catch block \(error.localizedDescription)")
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, transformed)
        XCTAssertEqual(assertions, 2)
    }

    func testPromiseRecoversFromErrorInChain() {

        let transformed = "XXX"
        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0
        let eerror: CodencityError = .urlCreationError
        var localSut: CodencityError?

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .chained { _ in

                assertions += 1
                return self.defaultsLoad(false)
            }
            .recover { error -> Future<String> in

                localSut = error
                assertions += 1
                return self.defaultsLoad(true, resolvesWith: transformed)
            }
            .observe { value in

                assertions += 1
                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, transformed)
        XCTAssertEqual(localSut, eerror)
        XCTAssertEqual(assertions, 3)
    }

    func testPromiseRetriesAndRecoversFromFailingChain() {

        let transformed = "XXX"
        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .chained { _ in

                assertions += 1
                return self.defaultsLoad(false)
            }
            .retry(count: 3, delay: 0.1) { () -> Future<String> in

                assertions += 1
                return self.defaultsLoad(true, resolvesWith: transformed)
            }
            .observe { value in

                assertions += 1
                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, transformed)
        XCTAssertEqual(assertions, 3)
    }

    func testPromiseRetriesExactlyNumberOfTimesFromFailingChain() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0
        let eerror: CodencityError? = .promiseRetryLimitReachedError
        var localSut: CodencityError?

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .retry(count: 3, delay: 0.1) { () -> Future<String> in

                assertions += 1
                return self.defaultsLoad(false)
            }
            .observe { _ in

                XCTFail("should not resolve the chain")
            }
            .catch { error in

                localSut = error
                expectation.fulfill()
            }

        waitForExpectations(timeout: 2, handler: nil)
        XCTAssertEqual(localSut, eerror)
        XCTAssertEqual(assertions, 3)
    }

    func testAlwaysFiresOnFailingChain () {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0
        let eerror: CodencityError = .urlCreationError
        var localSut: CodencityError?

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .chained { _ -> Future<String> in

                assertions += 1
                return self.defaultsLoad(false)
            }
            .observe { _ in

                XCTFail("should not observe when rejected")
            }
            .catch { error in

                assertions += 1
                XCTAssertEqual(error, eerror)
            }
            .always { _, error in

                assertions += 1
                localSut = error
                expectation.fulfill()
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, nil)
        XCTAssertEqual(localSut, eerror)
        XCTAssertEqual(assertions, 3)
    }

    func testAlwaysFiresOnResolvingChain() {

        let transformed = "XXX"
        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .chained { _ -> Future<String> in

                assertions += 1
                return self.defaultsLoad(true, resolvesWith: transformed)
            }
            .observe { value in

                XCTAssertEqual(value, transformed)
                assertions += 1
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }
            .always { string, _ in

                assertions += 1
                self.sut = string
                expectation.fulfill()
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, transformed)
        XCTAssertEqual(assertions, 3)
    }

    func testValidateClosurePreventsFailingChainPredicateFromContinuing() {

        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0
        let eerror: CodencityError = .promiseValidationError
        var localSut: CodencityError?

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .validate { _ -> Bool in

                assertions += 1
                return false
            }
            .observe { _ in

                XCTFail("should not observe when predicate failed")
            }
            .catch { error in

                assertions += 1
                localSut = error
                expectation.fulfill()
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(localSut, eerror)
        XCTAssertEqual(assertions, 2)
    }

    func testValidateClosureAllowsSucceedingChainPredicateToContinuing() {

        let transformed = "XXX"
        let outcome = "foo"
        let expectation = self.expectation(description: "promise")
        var assertions = 0

        _ = Promise<String> { future in

                self.delay(0.1) {
                    future.resolve(with: outcome)
                }
            }
            .validate { value -> Bool in

                assertions += 1
                return value == outcome
            }
            .chained { _ -> Future<String> in

                assertions += 1
                return self.defaultsLoad(true, resolvesWith: transformed)
            }
            .observe { value in

                assertions += 1
                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, transformed)
        XCTAssertEqual(assertions, 3)
    }

    func testDelayClosureWorksAsExected() {

        let string = "foo"
        let transformed = "XXX"
        var startTime: CFAbsoluteTime?
        let expectation = self.expectation(description: "promise")
        var assertions = 0

        _ = Promise<String> { future in

                self.delay(0.1) {

                    startTime = CFAbsoluteTimeGetCurrent()
                    future.resolve(with: string)
                }
            }
            .delay(3)
            .chained { _ -> Future<String> in

                assertions += 1
                return self.defaultsLoad(true, resolvesWith: transformed)
            }
            .validate { _ -> Bool in

                guard let time = startTime else {
                    return false
                }

                assertions += 1
                let timeElapsed = CFAbsoluteTimeGetCurrent() - time

                return timeElapsed > 3
            }
            .observe { value in

                assertions += 1
                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 4, handler: nil)
        XCTAssertEqual(self.sut, transformed)
        XCTAssertEqual(assertions, 3)
    }

    func testRaceClosureWorksAsExpected() {

        let firstString = "uno"
        let secondString = "due"
        let expectation = self.expectation(description: "promise")

        let first = Promise<String> { future in

            self.delay(0.5) {

                future.resolve(with: firstString)
            }
        }

        let second = Promise<String> { future in

            self.delay(0.2) {

                future.resolve(with: secondString)
            }
        }

        Promise
            .race(first, second)
            .observe { value in

                self.sut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 2, handler: nil)
        XCTAssertEqual(self.sut, secondString)
    }

    func testTimeoutWillBlockChainFromAdvancing() {

        let string = "foo"
        let expectation = self.expectation(description: "promise")
        let eerror: CodencityError = .promiseExecutionTimedOut
        var localSut: CodencityError?

        Promise<String> { future in

            self.delay(1) {

                future.resolve(with: string)
            }
        }
        .timeout(0.3)
        .observe { _ in

            XCTFail("should not observe when timeout reached")
        }
        .catch { error in

            localSut = error
            expectation.fulfill()
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(localSut, eerror)
    }

    func testTimeoutIsBeatenByFasterPromise() {

        let string = "uno"
        let expectation = self.expectation(description: "promise")

        Promise<String> { future in

            self.delay(0.1) {

                future.resolve(with: string)
            }
        }
        .timeout(2)
        .observe { value in

            self.sut = value
            expectation.fulfill()
        }
        .catch { error in

            XCTFail("should not fail to catch block \(error.localizedDescription)")
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(self.sut, string)
    }

    func testAllClosureWorksAsExpected() {

        let firstString = "uno"
        let secondString = "due"
        let expectation = self.expectation(description: "promise")
        var localSut: Array<String>?

        let first = Promise<String> { future in

            self.delay(0.3) {

                future.resolve(with: firstString)
            }
        }

        let second = Promise<String> { future in

            self.delay(0.1) {

                future.resolve(with: secondString)
            }
        }

        Promise.all(first, second)
            .observe { value in

                localSut = value
                expectation.fulfill()
            }
            .catch { error in

                XCTFail("should not fail to catch block \(error.localizedDescription)")
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(localSut, [firstString, secondString])
    }

    func testAllClosureCatchesTheChainPartFailingOnFirst() {

        let firstString = "uno"
        let expectation = self.expectation(description: "promise")
        let eerror: CodencityError = .promiseRetryLimitReachedError
        var localSut: CodencityError?

        let first = Promise<String> { future in

            self.delay(0.3) {

                future.resolve(with: firstString)
            }
        }

        let second = Promise<String> { future in

            self.delay(0.1) {

                future.reject(with: .promiseRetryLimitReachedError)
            }
        }

        _ = Promise<String> { _ in

            Promise.all(first, second)
                .observe { _ in

                    XCTFail("should not observe when timeout reached")
                }
                .catch { error in

                    localSut = error
                    expectation.fulfill()
            }
        }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(localSut, eerror)
    }

    func testAllClosureCatchesTheChainPartFailingOnLast() {

        let firstString = "uno"
        let expectation = self.expectation(description: "promise")
        let eerror: CodencityError = .promiseRetryLimitReachedError
        var localSut: CodencityError?

        let first = Promise<String> { future in

            self.delay(0.3) {

                future.reject(with: .promiseRetryLimitReachedError)
            }
        }

        let second = Promise<String> { future in

            self.delay(0.1) {

                future.resolve(with: firstString)
            }
        }

        _ = Promise<String> { _ in

            Promise.all(first, second)
                .observe { _ in

                    XCTFail("should not observe when timeout reached")
                }
                .catch { error in

                    localSut = error
                    expectation.fulfill()
                }
            }

        waitForExpectations(timeout: 0.5, handler: nil)
        XCTAssertEqual(localSut, eerror)
    }

    func testZipClosureFinishesAfterSuccess() {

        let firstString = "uno"
        let firstNumber = 42
        let expectation = self.expectation(description: "promise")

        let first = Promise<String> { future in

            self.delay(0.5) {

                future.resolve(with: firstString)
            }
        }

        let second = Promise<Int> { future in

            self.delay(0.2) {

                future.resolve(with: firstNumber)
            }
        }

        Promise<(String, Int)>
            .zip(first, second)
            .observe { (text, number) in

                XCTAssertEqual(text, firstString)
                XCTAssertEqual(number, firstNumber)
                expectation.fulfill()
            }
            .catch { _ in

                XCTFail("should not fail")
            }

        waitForExpectations(timeout: 2, handler: nil)
    }

    func testZipClosureFailsAfterOneFailure() {

        let expectation = self.expectation(description: "promise")
        let eerror: CodencityError = .promiseRetryLimitReachedError
        var localSut: CodencityError?
        let firstNumber = 42

        let first = Promise<String> { future in

            self.delay(0.5) {

                future.reject(with: eerror)
            }
        }

        let second = Promise<Int> { future in

            self.delay(0.2) {

                future.resolve(with: firstNumber)
            }
        }

        Promise<(String, Int)>
            .zip(first, second)
            .observe { (text, number) in

                XCTFail("should not succeed")
            }
            .catch { error in

                localSut = error
                expectation.fulfill()
            }

        waitForExpectations(timeout: 2, handler: nil)
        XCTAssertEqual(localSut, eerror)
    }

    func testZipClosureFailsAfterTwoFailures() {

        let expectation = self.expectation(description: "promise")
        let eerror: CodencityError = .promiseRetryLimitReachedError
        var localSut: CodencityError?

        let first = Promise<String> { future in

            self.delay(0.5) {

                future.reject(with: eerror)
            }
        }

        let second = Promise<Int> { future in

            self.delay(0.2) {

                future.reject(with: eerror)
            }
        }

        Promise<(String, Int)>
            .zip(first, second)
            .observe { (text, number) in

                XCTFail("should not succeed")
            }
            .catch { error in

                localSut = error
                expectation.fulfill()
            }

        waitForExpectations(timeout: 2, handler: nil)
        XCTAssertEqual(localSut, eerror)
    }

    func testZipClosureResolvesWithNoDelay() {

        let firstString = "uno"
        let firstNumber = 42
        let expectation = self.expectation(description: "xx")

        let first = Promise<String> { future in

            future.resolve(with: firstString)
        }

        let second = Promise<Int> { future in

            future.resolve(with: firstNumber)
        }

        Promise<(String, Int)>
            .zip(first, second)
            .observe { (text, number) in

                XCTAssertEqual(text, firstString)
                XCTAssertEqual(number, firstNumber)
                expectation.fulfill()
            }
            .catch { _ in

                XCTFail("should not fail")
            }

        waitForExpectations(timeout: 2, handler: nil)
    }
}
