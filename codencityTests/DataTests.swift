//
//  DataTests.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

@testable import codencityDev
import XCTest

final class DataTests: XCTestCase {

    var sut: String?

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {

        sut = nil
        super.tearDown()
    }

    func testDataHandlesValidJson() {

        let expectation = self.expectation(description: "Promise")

        sut = "{ \"message\": \"Bad credentials\", \"documentation_url\": \"https://developer.github.com/v3\" }"

        sut?
            .data(using: .utf8)?
            .decodeJSON()
            .observe { value in

                guard let val = value as? Dictionary<String, AnyObject> else {

                    XCTFail(">>> failed to decode JSON")
                    return
                }

                let mockDict = ["message": "Bad credentials" as AnyObject,
                                "documentation_url": "https://developer.github.com/v3" as AnyObject]

                XCTAssertTrue(val == mockDict, "value returned from decodeJSON should be equal to mockDict")
                expectation.fulfill()
            }
            .catch { error in
                XCTFail(">>> failed to decode JSON \(error)")
            }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testDataHandlesInvalidJson() {

        let expectation = self.expectation(description: "Promise")

        sut = "{ { { { { \"message\": \"Bad credentials\", \"documentation_url\": \"https://developer.github.com/v3\" }"
        let jsonData = sut?.data(using: .utf8)
        let errorString = "failed to decode: Optional(\"{ { { { { \\\""
                        + "message\\\": \\\"Bad credentials\\\", \\\""
                        + "documentation_url\\\": \\\"https://developer"
                        + ".github.com/v3\\\" }\")"

        jsonData?
            .decodeJSON()
            .observe { _ in

                XCTFail(">>> value returned from decodeJSON should be nil for invalid json")
            }
            .catch { error in

                XCTAssertEqual(error,
                               .jsonCreationError(message: errorString),
                               "value returned from decodeJSON should be equal to mockDict")
                expectation.fulfill()
            }

        waitForExpectations(timeout: 5, handler: nil)
    }
}
