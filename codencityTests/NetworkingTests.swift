//
//  NetworkingTests.swift
//  codencityTests
//
//  Created by tom Hastik on 19/04/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

@testable import codencityDev
import XCTest

final class NetworkingTests: XCTestCase, Networking {

    func testSendReturnsValidData() {

        let expectation = self.expectation(description: "Promise")

        let mockData = APITests().fromMockFile("getRepos")
        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 200,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (mockData, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        NetworkingTests
            .send(session, packet: .getRepositories(user: settings.githubUser))
            .observe { result in

                guard let response = result as? Array<JSON> else { return }

                XCTAssertEqual(30,
                               response.count,
                               "array should have 30 elements when loaded from mocked json data")

                expectation.fulfill()
            }
            .catch { error in

                print(">>> 💥 \(#file) \(#function) \(#line) \(error)")
                XCTFail("this should not happen")
            }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testSendReturnsValidErrorForBadStatusCode() {

        let expectation = self.expectation(description: "Promise")

        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 500,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (nil, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        NetworkingTests
            .send(session, packet: .getRepositories(user: settings.githubUser))
            .observe { _ in

                XCTFail("this should not happen")
            }
            .catch { error in

                XCTAssertEqual(error,
                               .networkError(message: "N/A", code: 500),
                               "networking should detect network error")
                expectation.fulfill()

            }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testSendReturnsValidErrorForEmptyData() {

        let expectation = self.expectation(description: "Promise")

        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 200,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (nil, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        NetworkingTests
            .send(session, packet: .getRepositories(user: settings.githubUser))
            .observe { _ in

                XCTFail("this should not happen")

            }
            .catch { error in
                XCTAssertEqual(error,
                               .networkEmptyData,
                               "networking should detect empty data error")
                expectation.fulfill()
            }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testSendReturnsValidErrorForNoResponse() {

        let expectation = self.expectation(description: "Promise")

        MockSession.mockResponse = (nil, urlResponse: nil, error: nil)
        let session = MockSession.shared

        NetworkingTests
            .send(session, packet: .getRepositories(user: settings.githubUser))
            .observe { _ in

                XCTFail("this should not happen")

            }
            .catch { error in

                XCTAssertEqual(error,
                               .unknownError(msg: "emptyResponse"),
                               "networking should detect empty response error")
                expectation.fulfill()
            }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testSendReturnsValidErrorForMalformedJSON() {

        let expectation = self.expectation(description: "Promise")

        let mockData = "{".data(using: .utf8)
        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 200,
                                           httpVersion: nil,
                                           headerFields: nil)

        MockSession.mockResponse = (mockData, urlResponse: mockResponse, error: nil)
        let session = MockSession.shared

        NetworkingTests
            .send(session, packet: .getRepositories(user: settings.githubUser))
            .observe { _ in

                XCTFail("this should not happen")

            }
            .catch { error in

                XCTAssertEqual(error,
                               .jsonCreationError(message: "failed to decode: Optional(\"{\")"),
                               "networking should detect empty response error")
                expectation.fulfill()
            }

        waitForExpectations(timeout: 5, handler: nil)
    }

    func testSendReturnsOtherNetworkFailure() {

        let expectation = self.expectation(description: "Promise")

        let mockResponse = HTTPURLResponse(url: "https://api.github.com",
                                           statusCode: 600,
                                           httpVersion: nil,
                                           headerFields: nil)

        let mockError = NSError(domain: "MockedNetworkFailure", code: 0, userInfo: nil)
        let mockErrorString = "The operation couldn’t be completed. (MockedNetworkFailure error 0.)"
        let mockResponseError: CodencityError = .networkError(message: mockErrorString, code: 600)

        MockSession.mockResponse = (nil, urlResponse: mockResponse, error: mockError)
        let session = MockSession.shared

        NetworkingTests
            .send(session, packet: .getRepositories(user: settings.githubUser))
            .observe { _ in

                XCTFail("output of sendPacket() function should fail for no data")

            }
            .catch { error in

                XCTAssertEqual(error,
                               mockResponseError,
                               "output of sendPacket() function should fail for no data")
                expectation.fulfill()
            }

        waitForExpectations(timeout: 5, handler: nil)
    }
}
