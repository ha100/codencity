//
//  DataSourceTests.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

@testable import codencityDev
import XCTest

class DataSourceTests: XCTestCase {

    var sut: CustomDataSource<String>?

    override func setUp() {

        super.setUp()
        sut = CustomDataSource<String>("testingIdentifier")
    }

    override func tearDown() {

        sut = nil
        super.tearDown()
    }

    func testDataSourceSubscriptReturnsValidObject() {

        sut?.contents = ["test", "this", "class", "successfully"]
        XCTAssertTrue(sut?[3] == "successfully", "dataSource object on third position should be equal to 'successfully'")
    }

    func testDataSourceIdentifierIsValid() {

        sut = CustomDataSource<String>("overriden")
        XCTAssertTrue(sut?.cellIdentifier == "overriden", "dataSource identifier should be equal to 'overriden'")
    }
}
