//
//  DiffMock.swift
//  codencityTests
//
//  Created by tom Hastik on 12/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//
//  Credits: Matias Cudich https://github.com/mcudich/HeckelDiff

import Foundation

struct DiffMock: Hashable {

    let value: Int
    let eValue: Int

    func hash(into hasher: inout Hasher) {
        hasher.combine(value.hashValue)
    }
}

func == (lhs: DiffMock, rhs: DiffMock) -> Bool {
    return lhs.eValue == rhs.eValue
}

func == (lhs: (from: Int, to: Int), rhs: (from: Int, to: Int)) -> Bool {
    return lhs.0 == rhs.0 && lhs.1 == rhs.1
}
