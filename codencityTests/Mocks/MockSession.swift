//
//  MockSession.swift
//  codencityTests
//
//  Created by tom Hastik on 19/04/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import Foundation

final class MockSession: URLSession {

    // MARK: - Properties

    var completionHandler: ((Data?, URLResponse?, Error?) -> Void)?

    static var mockResponse: (data: Data?, urlResponse: URLResponse?, error: Error?) = (data: nil, urlResponse: nil, error: nil)

    override class var shared: URLSession {
        return MockSession()
    }

    // MARK: - Functions

    override func dataTask(with request: URLRequest,
                           completionHandler: ((Data?, URLResponse?, Error?) -> Void)?) -> URLSessionDataTask {

        self.completionHandler = completionHandler
        return MockTask(response: MockSession.mockResponse, completionHandler: completionHandler)
    }
}

final class MockTask: URLSessionDataTask {

    typealias Response = (data: Data?, urlResponse: URLResponse?, error: Error?)

    // MARK: - Properties

    var mockResponse: Response

    let completionHandler: ((Data?, URLResponse?, Error?) -> Void)?

    // MARK: - Init

    init(response: Response, completionHandler: ((Data?, URLResponse?, Error?) -> Void)?) {

        self.mockResponse = response
        self.completionHandler = completionHandler
    }

    // MARK: - Functions

    override func resume() {
        completionHandler?(mockResponse.data, mockResponse.urlResponse, mockResponse.error)
    }
}
