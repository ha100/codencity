//
//  MockReporter.swift
//  codencityTests
//
//  Created by tom Hastik on 03/08/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

@testable import codencityDev
import XCTest

final class MockReporter: AnalyticsProvider {

    var methodCalled = false

    func report(event: AnalyticsEvent) {
        methodCalled = true
    }
}
