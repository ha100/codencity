# codencity
dense code in the city

[Documentation](https://ha100.github.io/index.html)

### ui

i guess the first thing would be to confess *"father, i have my reasons to do all ui in code"* :) and minimize the use of xibs to an absolute minimum and refuse to use storyboards at all. yes, they are nice for prototyping but

 1) interface builder takes ages to start (accidentally click that storyboard file while scrolling through files in navigator and that *"code you're not supposed to do on main thread"* will not let you work for seconds)
 2) is not available at linux and in vim
 3) are mess to resolve merge conflicts in and always litter your commits with garbage. and so i refuse to scroll interactively via 100 useless frame changes in one xml with every commit just because you visited your favorite ui file
 4) i always struggle to find the place that one switch for visibility or color is located on anyway. you can always search and destroy in code easily

 if you are on the other side and like to use them, i don't judge you and it's perfectly fine. i might give you a weird look :), but i don't judge you.

### arch

this piece will be based around the coordinators with some glimpses of mvvm - and will try to keep the controllers slim.

### networking

as for the [networking](x-source-tag://networking), i don't really like to use frameworks that much even if i like their coding style. first, i like to know about every single line of code in my codebase (i can hear you all brag about reinventing the wheel) and second there is always a ton of code you will never use. not to mention that rolling out your own micro networking will force you to learn more about the inner workings of the network protocols and some architecture. headers, agents, cookies, query parameters, response codes, etag, redirects and all sorts of other interesting stuff. just roll up your sleeves and get into it. it was networking that finally taught me delegates, closures and promises. it is enriched with [certificate pinning](x-source-tag://certificatePinning) to add a bit of security on top. it is using RESTful API with JSON at the moment, but that is just for this presentation only. i am moving more towards the use of protocol buffers and flat buffers so the codebase will reflect this in the future.

###### mocked responses

to setup mock response we need to
1) install and run [mitmproxy](https://mitmproxy.org)
2) capture some comms from github with curl
3) replay data to app

this way our app will not hit github api and deplete our quota because the response will come directly from our proxy. the setup will look like this.

```
                                 ┌───────────────────────┐
                                 │  ┌─────────────────┐  │
                                 │  │ 2)   curl       │  │
                                 │  └─────────────────┘  │
                                 │           │           │
                                 │       requests        │
                                 │         repos         │
 ┌───────────────┐               │           │           │
 │      3)       │               │           ▼           │             ┌──────────────┐
 │   codenCity   │  repo request │  ┌─────────────────┐  │             │              │
 │  iPhone  app  │◀──────────────┼─▶│ 1) mitmProxy    │◀─┼─interwebz──▶│  gitHub.com  │
 │               │ response from │  └─────────────────┘  │             │              │
 └───────────────┘   our proxy   │                      │             └──────────────┘
                                 └───────────────────────┘
```

there are multiple ways how to achieve this, but for now we will dump the sample data from github by running the proxy in reverse mode on port 8080 with command

```lang=bash
$mitmdump -w githubApi.dump -p 8080 --mode reverse:https://api.github.com:443/
```

we then fire the repo request with curl via the running proxy with

```lang=bash
$curl http://localhost:8080/users/ha100/repos --insecure
```

after that we can stop the proxy and run it in replay mode with the captured data from the file. the app networking will try to connect and get data from the running [proxy](x-source-tag://mockedUrl)

```lang=bash
$mitmdump -S githubApi.dump --mode reverse:https://api.github.com --listen-host 0.0.0.0 -p 8080 --set server_replay_nopop -k
```

this is now optimized for default use with simulator and proxy running on the same host. if you would like to test the proxy with the app running on the device, uncomment the script in build phases and run the mitmproxy with this command

```lang=bash
$mitmdump -S githubApi.dump --mode reverse:https://api.github.com --listen-host `ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}'` -p 8080 --set server_replay_nopop -k
```

if everything goes as expected, you should see the sample data on the screen of the app and output log from in proxy similar to this

```
192.168.1.3:53273: clientconnect
192.168.1.3:53273: GET https://api.github.com/users/ha100/repos
[replay]                 << 200 OK 159.36k
192.168.1.3:53273: clientdisconnect
```

### extensions

the tableViews use [generic dataSource](x-source-tag://dataSource) and cells are [dequeued](x-source-tag://cellDeque) via extension to make code more readable. [cell registration](x-source-tag://cellRegistration) via extension for readability in the controller and [cell setup](x-source-tag://cellSetup) is moved from the usual place in delegate method to the cell object in order to keep the stuff encapsulated. [string localization](x-source-tag://stringLocalization) is done via extension even that i failed to be able to export all the strings for the translation once. there is heavy use of the [lazy properties](x-source-tag://lazySetup) for their setup via extension to be more separated from the rest of the code. and buttons use [closure](x-source-tag://targetButtonClosure) to setup their target actions and keep the code more close to the caller.

### security

apart from the network layer being able to handle [certificate pinning](x-source-tag://certificatePinning), there is basic anticrack check in release target that will detach the debugger at app start.

### xcode setup

track xcode build time

```lang=bash
defaults write com.apple.dt.Xcode ShowBuildOperationDuration -bool YES
```

### future plans

i will probably add some crypto example for protecting the communication and some more anti cracking measures in the future. it would be nice to have some api pagination logic implemented too.


that's all folks\
happy crocheting



## references

* Callout(Lighter View Controllers):
[*https://www.objc.io/issues/1-view-controllers/lighter-view-controllers/*](https://www.objc.io/issues/1-view-controllers/lighter-view-controllers/)
\
\
Chris Eidhof [@chriseidhof](https://twitter.com/chriseidhof)
#
* Callout(Protocol-Oriented Programming in Swift):
[*https://www.youtube.com/watch?v=g2LwFZatfTI*](https://www.youtube.com/watch?v=g2LwFZatfTI)
\
\
Dave Abrahams [@daveabrahams](https://twitter.com/daveabrahams)
#
* Callout(Practical Protocol-Oriented):
[*https://www.youtube.com/watch?v=qeDxb_ucqr0*](https://www.youtube.com/watch?v=qeDxb_ucqr0)
\
\
Natasha Murashev [@natashatherobot](https://twitter.com/natashatherobot)
#
* Callout(Using Generics to improve TableView cells):
[*http://alisoftware.github.io/swift/generics/2016/01/06/generic-tableviewcells/*](http://alisoftware.github.io/swift/generics/2016/01/06/generic-tableviewcells/)
\
\
Olivier Halligon [@aligatr](https://twitter.com/aligatr)
#
* Callout(Low-Hanging View Controller Fruits):
[*https://engineers.sg/video/low-hanging-view-controller-fruits-ios-conf-sg-2016--1212*](https://engineers.sg/video/low-hanging-view-controller-fruits-ios-conf-sg-2016--1212)
\
\
Gwen Weston [@Purpleyay](https://twitter.com/Purpleyay)
#
* Callout(Presenting Coordinators):
[*https://vimeo.com/144116310*](https://vimeo.com/144116310)
\
\
Soroush Khanlou [@khanlou](https://twitter.com/khanlou)
#
* Callout(Protocol Oriented ViewController Coordinators):
[*https://www.youtube.com/watch?v=KDl7Czw63mM*](https://www.youtube.com/watch?v=KDl7Czw63mM)
\
\
Niels van Hoorn [@nvh](https://twitter.com/nvh)
#
* Callout(Discovering Native Swift Patterns):
[*https://realm.io/news/slug-nick-oneill-native-swift-patterns/*](https://realm.io/news/slug-nick-oneill-native-swift-patterns/)
\
\
Nick O’Neill [@nickoneill](https://twitter.com/nickoneill)
#
* Callout(Diff Localizable.strings):
[*https://gist.github.com/asmallteapot/11227602*](https://gist.github.com/asmallteapot/11227602)
\
\
Ellen Teapot [@asmallteapot](https://github.com/asmallteapot)
#
* Callout(Improving completion blocks in Swift):
[*https://www.novoda.com/blog/improving-completion-blocks-in-swift/*](https://www.novoda.com/blog/improving-completion-blocks-in-swift/)
\
\
Alex Curran [@amlcurran](https://twitter.com/amlcurran)
#
* Callout(Result Type):
[*https://github.com/antitypical/Result*](https://github.com/antitypical/Result)
\
\
Rob Rix [@rob_rix](https://twitter.com/rob_rix)
#
* Callout(Certificate Pinning):
[*https://talk.objc.io/episodes/S01E57-certificate-pinning*](https://talk.objc.io/episodes/S01E57-certificate-pinning)
\
\
Rob Napier [@cocoaphony](https://twitter.com/cocoaphony) & Florian Kugler [@floriankugler](https://twitter.com/floriankugler)
#
* Callout(How to Easily Switch Your App Delegate for Testing):
[*https://qualitycoding.org/app-delegate-for-tests/*](https://qualitycoding.org/app-delegate-for-tests/)
\
\
Jon Reid [@qcoding](https://twitter.com/qcoding)
#
* Callout(URL init extension):
[*https://twitter.com/johnsundell/status/886876157479616513*](https://twitter.com/johnsundell/status/886876157479616513)
\
\
John Sundell [@johnsundell](https://twitter.com/johnsundell)
#
* Callout(My favorite Swift extensions):
[*https://dev.to/tittn/my-favorite--swift-extensions-8g7*](https://dev.to/tittn/my-favorite--swift-extensions-8g7)
\
\
Tatsuya Tanaka [@tanakasan2525](https://twitter.com/tanakasan2525)
#
* Callout(Some useful URL schemes in Xcode 9):
[*https://cocoaengineering.com/2018/01/01/some-useful-url-schemes-in-xcode-9/*](https://cocoaengineering.com/2018/01/01/some-useful-url-schemes-in-xcode-9/)
\
\
Daniel Martín [@dmartincy](https://twitter.com/dmartincy)
#
* Callout(Adding a closure as target to a UIButton):
[*https://stackoverflow.com/a/41438789/2027018*](https://stackoverflow.com/a/41438789/2027018)
\
\
Joe Charlier [@aepryus](https://stackoverflow.com/users/126541/aepryus)
#
* Callout(Rendering Markdown in Xcode 9):
[*http://danielinoa.com/xcode/2017/09/19/markdown-in-xcode9.html*](http://danielinoa.com/xcode/2017/09/19/markdown-in-xcode9.html)
\
\
Daniel Inoa [@danielinoa_](https://twitter.com/danielinoa_)
#
* Callout(Table view cell load animation one after another):
[*https://stackoverflow.com/a/46928194/2027018*](https://stackoverflow.com/a/46928194/2027018)
\
\
chikko [@chikko](https://stackoverflow.com/users/2140040/chikko)
#
* Callout(Pure Swift implementation of Paul Heckel's A Technique for Isolating Differences Between Files):
[*https://github.com/mcudich/HeckelDiff*](https://github.com/mcudich/HeckelDiff)
\
\
Matias Cudich [@mcudich](https://github.com/mcudich/HeckelDiff)

