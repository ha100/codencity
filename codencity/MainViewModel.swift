//
//  MainViewModel.swift
//  codencity
//
//  Created by tom Hastik on 25/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import Foundation

final class MainViewModel: MVVMable {

    // MARK: - Properties

    var model: CustomDataSource<Repo>? = CustomDataSource<Repo>(CustomTableCell.reuseIdentifier)

    weak var networkDelegate: NetworkObservable?

    // MARK: - Init

    init(delegate: NetworkObservable) {

        self.networkDelegate = delegate
        self.obtainData()
    }
}

extension MainViewModel: DataRetrievable {

    func obtainData() {

        API
            .getRepos(for: settings.githubUser)
            .observe { repos in

                let update = ListUpdate(diff(self.model?.contents ?? [], repos))

                self.model?.contents = repos

                DispatchQueue.main.async {
                    self.networkDelegate?.receivedNetworkData(data: update)
                }
            }
    }
}

extension ListUpdate: NetworkPresentable {}
