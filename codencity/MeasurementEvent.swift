//
//  MeasurementEvent.swift
//  codencity
//
//  Created by tom Hastik on 29/06/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import Foundation

public enum MeasurementEvent: String {

    case appLaunch
}
