//
//  UIControl+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//
//  Credits: https://stackoverflow.com/a/41438789/2027018

import UIKit

final class ClosureSleeve {

    let closure: () -> Void

    init (_ closure: @escaping () -> Void) {
        self.closure = closure
    }

    @objc
    func invoke () {
        closure()
    }
}

extension UIControl {

    /// simplified UIButton target actions
    ///
    /// - Parameters:
    ///   - controlEvents: events to handle
    ///   - closure: actions to be done upon trigger
    ///
    /// this allows button action to be prepared with
    ///````
    ///let button = UIButton(type: .custom)
    ///button.add(for: [.touchUpInside]) { [weak self] in
    ///⋮
    ///}
    ///````
    ///
    /// rather than with
    ///
    ///````
    ///let button = UIButton(type: .custom)
    ///button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    ///
    ///@objc func buttonAction(sender: UIButton!) {
    ///⋮
    ///}
    ///````
    /// - Tag: targetButtonClosure
    func add(for controlEvents: UIControl.Event, _ closure: @escaping () -> Void) {

        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        objc_setAssociatedObject(self, String(format: "[%d]", arc4random()), sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}
