//
//  MainCoordinator.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class MainCoordinator: NSObject {

    // MARK: - Properties

    var root: UINavigationController
    var viewController: UIViewController?

    var detailCoordinator: DetailCoordinator?

    // MARK: - Init

    init(root: UINavigationController) {

        self.root = root
        super.init()

        self.root.delegate = self
    }
}

extension MainCoordinator: MainPresentable {

    func settingsButtonTapped() {

        switch self.root.topViewController {

            case is SettingsViewController:

                guard let controller = viewController as? MainViewController else {

                    analytics.report(event: .controllerFailure(file: #file, function: #function, line: #line))
                    return
                }

                self.root.pushViewController(controller, animated: true)

            default: self.root.popViewController(animated: true)
        }
    }

    func cellAction(with repo: Repo) {

        detailCoordinator = DetailCoordinator(root: root, repo: repo)
        detailCoordinator?.start()

        guard let controller = self.viewController as? MainViewController else {

            analytics.report(event: .controllerFailure(file: #file, function: #function, line: #line))
            return
        }

        controller.settingsButton.isHidden = true
    }
}

extension MainCoordinator: UINavigationControllerDelegate {

    func navigationController(_ navigationController: UINavigationController,
                              didShow viewController: UIViewController,
                              animated: Bool) {

        if viewController is MainViewController {
            self.detailCoordinator = nil
        }
    }
}

extension MainCoordinator: Coordinating {

    func createViewController() -> UIViewController {
        return MainViewController(delegate: self)
    }

    func configure(_ viewController: UIViewController) {

        guard let controller = viewController as? MainViewController else {

            analytics.report(event: .controllerFailure(file: #file, function: #function, line: #line))
            return
        }

        controller.navigationItem.hidesBackButton = true

        self.root.viewControllers = [SettingsViewController(), controller]
        self.root.navigationBar.addSubview(controller.settingsButton)
    }

    func start() {

        let controller = createViewController()
        viewController = controller
        configure(controller)
    }
}

extension MainCoordinator: NavigationCoordinating {}
