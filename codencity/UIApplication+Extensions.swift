//
//  UIApplication+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 24/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import UIKit

extension UIApplication {

    /// obtain the top most UIViewController
    ///
    var topViewController: UIViewController? {

        guard var topViewController = UIApplication.shared.keyWindow?.rootViewController else {

            analytics.report(event: .controllerFailure(file: #file, function: #function, line: #line))
            return nil
        }

        while let presentedViewController = topViewController.presentedViewController {
            topViewController = presentedViewController
        }

        return topViewController
    }

    /// obtain the top most UINavigationController
    ///
    var topNavigationController: UINavigationController? {
        return topViewController as? UINavigationController
    }
}
