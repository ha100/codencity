//
//  disableAttach.c
//  codencityRelease
//
//  Created by tom Hastik on 06/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

#include <stdio.h>
#import <dlfcn.h>

typedef int (*command_ptr_t)(int _request, pid_t _pid, caddr_t _addr, int _data);

#if !defined(PT_DENY_ATTACH)
#define PT_DENY_ATTACH 31
#endif

void disable_attach() {

    void *handle = dlopen(0, RTLD_GLOBAL | RTLD_NOW);
    command_ptr_t command_ptr = dlsym(handle, "ptrace");
    command_ptr(PT_DENY_ATTACH, 0, 0, 0);

    dlclose(handle);
}
