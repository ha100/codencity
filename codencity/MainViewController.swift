//
//  MainViewController.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class MainViewController: MVVMController<MainViewModel>, UITableViewDelegate {

    // MARK: - Properties

    var tableCellsLoaded = false

    lazy var settingsButton: UIButton = {

        UIButton(type: .custom).apply {

            $0.frame = CGRect(x: 0, y: 0, width: 55, height: 55)
            $0.isAccessibilityElement = true
            $0.accessibilityLabel = L10n.MainFlow.Controller.SettingsButton.accessibilityLabel.localized
            $0.setImage(Asset.menuButton.image, for: .normal)

            $0.add(for: [.touchUpInside]) { [weak self] in
                self?.coordinateDelegate?.settingsButtonTapped()
            }
        }
    }()

    lazy var tableView: UITableView = {

        UITableView(frame: .zero).apply {

            $0.translatesAutoresizingMaskIntoConstraints = false

            $0.insertSubview(self.refreshControl, at: 0)

            $0.backgroundView = self.refreshControl
            $0.dataSource = self.viewModel?.model
            $0.isAccessibilityElement = true
            $0.accessibilityLabel = L10n.MainFlow.Controller.TableView.accessibilityLabel.localized

            $0.register(CustomTableCell.self)

            $0.tableFooterView = UIView()
            $0.backgroundColor = .clear
            $0.delegate = self
        }
    }()

    lazy var refreshControl: UIRefreshControl = {

        UIRefreshControl().apply {

            $0.tintColor = UIColor.lightBlue
            $0.addTarget(self,
                         action: #selector(MainViewController.handleRefresh(_:)),
                         for: UIControl.Event.valueChanged)
        }
    }()

    lazy var progressIndicator: ActivityView = {

        ActivityView().apply {

            $0.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            $0.center = self.view.center
        }
    }()

    weak var coordinateDelegate: MainPresentable?

    // MARK: - Init

    convenience init(delegate: MainPresentable) {

        self.init(nibName: nil, bundle: nil)
        self.coordinateDelegate = delegate
        self.viewModel = MainViewModel(delegate: self)
    }

    // MARK: - UIView

    override func viewDidLoad() {

        super.viewDidLoad()

        self.view.backgroundColor = .lightGray
        self.view.add(tableView, progressIndicator)

        self.tableView.fillSuperview()
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        self.settingsButton.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @objc
    func handleRefresh(_ refreshControl: UIRefreshControl) {

        self.viewModel?.obtainData()
        refreshControl.endRefreshing()
    }

    // MARK: - UITableViewDelegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        guard let repo = self.viewModel?.model?[indexPath.row] else {

            print(">>> 💥 \(#file) \(#function) \(#line)")
            return
        }

        self.coordinateDelegate?.cellAction(with: repo)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        var lastCellLoaded = false

        if self.viewModel?.model?.count ?? 0 > 0 && !tableCellsLoaded {

            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {

                lastCellLoaded = true
            }
        }

        switch tableCellsLoaded {

            case false:

                if lastCellLoaded {
                    tableCellsLoaded = true
                }

                cell.transform = CGAffineTransform(translationX: 0, y: 65 / 2)
                cell.alpha = 0

                UIView.animate(withDuration: 0.5,
                               delay: 0.05 * Double(indexPath.row),
                               usingSpringWithDamping: 0.6,
                               initialSpringVelocity: 0.6,
                               options: .curveEaseInOut,
                               animations: {

                                cell.transform = CGAffineTransform(translationX: 0, y: 0)
                                cell.alpha = 1

                }, completion: { _ in })

            case true:

                var transform = CATransform3DIdentity
                transform.m34 = -1.0 / cell.bounds.size.width
                cell.layer.transform = transform
                cell.alpha = 0.6

                UIView.animate(withDuration: 0.33,
                               delay: 0,
                               usingSpringWithDamping: 0.6,
                               initialSpringVelocity: 0.6,
                               options: .curveEaseInOut,
                               animations: {

                                cell.layer.transform = CATransform3DMakeScale(0.9, 0.85, 1.0)
                                cell.layer.transform = CATransform3DIdentity
                                cell.alpha = 1

                }, completion: { _ in })
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {

        coordinator.animate(alongsideTransition: { _ in
            self.progressIndicator.center = self.view.center
        }) { _ in
        }

        super.viewWillTransition(to: size, with: coordinator)
    }
}

// MARK: - NetworkObservable

extension MainViewController: NetworkObservable {

    func receivedNetworkData(data: NetworkPresentable?) {

        guard let updates = data as? ListUpdate else {

            let event: AnalyticsEvent = .unexpectedObjectFailure(expecting: "\(ListUpdate.self)",
                                                                 got: "\(type(of: data))",
                                                                 file: #file,
                                                                 function: #function,
                                                                 line: #line)
            analytics.report(event: event)
            return
        }

        self.progressIndicator.removeFromSuperview()

        tableView.applyDiff(updates, withAnimation: .bottom)
    }
}
