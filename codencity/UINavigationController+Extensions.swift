//
//  UINavigationController+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 09/06/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import UIKit

extension UINavigationController {

    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
}
