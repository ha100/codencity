//
//  Constrainer.swift
//  codencity
//
//  Created by tom Hastik on 09/06/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import UIKit

final class Constrainer {

    // MARK: - Properties

    var contraints: Array<NSLayoutConstraint> = []
    let element: ConstrainedElement

    // MARK: - Init

    init(view: ConstrainedElement) {

        self.element = view
        setupConstraints()
    }

    // MARK: - Logic

    func setupConstraints() {

        switch self.element {

        case let .version(view):

            self.contraints = [ NSLayoutConstraint(item: view,
                                                   attribute: .leading,
                                                   relatedBy: .equal,
                                                   toItem: view.superview,
                                                   attribute: .leading,
                                                   multiplier: 1,
                                                   constant: 0),
                                NSLayoutConstraint(item: view,
                                                   attribute: .trailing,
                                                   relatedBy: .equal,
                                                   toItem: view.superview,
                                                   attribute: .trailing,
                                                   multiplier: 1,
                                                   constant: 0),
                                NSLayoutConstraint(item: view,
                                                   attribute: .height,
                                                   relatedBy: .equal,
                                                   toItem: nil,
                                                   attribute: .height,
                                                   multiplier: 1,
                                                   constant: 20),
                                NSLayoutConstraint(item: view,
                                                   attribute: .bottom,
                                                   relatedBy: .equal,
                                                   toItem: view.superview,
                                                   attribute: .bottom,
                                                   multiplier: 1,
                                                   constant: -20) ]
        }
    }
}

extension Constrainer {

    static func version(view: UIView) -> Array<NSLayoutConstraint> {
        return Constrainer(view: .version(view: view)).contraints
    }
}
