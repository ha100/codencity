//
//  NavigationCoordinating.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

protocol NavigationCoordinating: Coordinating {
    associatedtype RootViewController = UINavigationController
}

extension NavigationCoordinating {

    func show(_ viewController: Self.ViewController) {

        guard let navRoot = root as? UINavigationController else {

            analytics.report(event: .controllerFailure(file: #file, function: #function, line: #line))
            return
        }

        navRoot.pushViewController(viewController, animated: true)
    }

    func dismiss() {

        guard let navRoot = root as? UINavigationController else {

            analytics.report(event: .controllerFailure(file: #file, function: #function, line: #line))
            return
        }

        navRoot.popViewController(animated: true)
    }
}
