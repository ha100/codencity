//
//  PhantomProtocols.swift
//  codencity
//
//  Created by tom Hastik on 30/06/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import Foundation

// phantom protocol for sourcery responsible for enum equality autogeneration
protocol AutoEquatable {}

// phantom protocol for sourcery responsible for enum equality autogeneration
protocol AutoCaseName {}
