//
//  ConstrainedElement.swift
//  codencity
//
//  Created by tom Hastik on 09/06/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import UIKit

enum ConstrainedElement {

    case version(view: UIView)
}
