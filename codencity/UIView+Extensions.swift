//
//  UIView+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 19/05/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import UIKit

extension UIView {

    func add(_ subviews: UIView...) {
        subviews.forEach(addSubview)
    }

    /// attach constraints and fill the whole screen
    func fillSuperview() {

        guard let superview = self.superview else {

            print(">>> 💥 constraints: failed to obtain superView")
            return
        }

        translatesAutoresizingMaskIntoConstraints = superview.translatesAutoresizingMaskIntoConstraints

        guard !translatesAutoresizingMaskIntoConstraints else {

            autoresizingMask = [.flexibleWidth, .flexibleHeight]
            frame = superview.bounds
            return
        }

        topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
        leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
        rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
    }

    /// obtain a view controller which has the view
    var viewController: UIViewController? {

        var parent: UIResponder? = self

        while parent != nil {

            parent = parent?.next

            if let viewController = parent as? UIViewController {
                return viewController
            }
        }

        return nil
    }
}
