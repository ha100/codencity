//
//  Collection+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 24/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

extension Collection {

    /// Get an element in safety.
    ///
    /// - Parameter index: index of the element
    subscript(safe index: Index) -> Element? {
        return startIndex <= index && index < endIndex ? self[index] : nil
    }
}
