//
//  UINavigationBar+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

extension UINavigationBar {

    static let navBarHeight: CGFloat = 60
    static let statusBarHeight: CGFloat = 20

    open override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: UINavigationBar.navBarHeight)
    }
}
