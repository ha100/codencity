//
//  SettingsPresentable.swift
//  codencity
//
//  Created by tom Hastik on 25/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

protocol SettingsPresentable: class {

    func settingsButtonTapped()
}

protocol DetailPresentable: class {

    func cellAction(with repo: Repo)
}

protocol MainPresentable: SettingsPresentable & DetailPresentable {}
