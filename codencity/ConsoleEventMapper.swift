//
//  ConsoleEventMapper.swift
//  codencity
//
//  Created by tom Hastik on 29/06/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import Foundation

public final class ConsoleEventMapper {

    func eventName(for event: AnalyticsEvent) -> String {

        switch event {

            // MARK: - appDelegate

            case .appBecomeActive: return "📱 app became active: v\(Settings.appVersion)"

            case .appResignActive: return "📱 app resigned active"

            case .appEnterBackground: return "📱 app entered background"

            case .appTerminate: return "📱 app terminated"

            case let .appMeasurement(time, event): return "⏱ \(event.rawValue) \(time) seconds"

            case .appMeasurementNoStartTime: return "💥 ⏱ appMeasurement needs start time"

            case let .cellDequeueFailure(identifier): return "💥 cell culd not dequeue with identifier \(identifier)"

            case let .controllerFailure(file, function, line): return "💥 controller \(file) \(function) \(line)"

            case let .deinit(object): return "☠️ deinit \(object)"

            case let .fontFailure(named, file, function, line): return "💥 font \(named) \(file) \(function) \(line)"

            case let .subclassingFailure(object): return "☠️ \(object) cannot be created and needs to be subclassed"

            case let .unexpectedObjectFailure(expecting, received, file, function, line):

                return "💥 expecting \(expecting) got \(received) \(file) \(function) \(line)"

            case let .urlFailure(link, file, function, line): return "💥 🌐 \(link) \(file) \(function) \(line)"
        }
    }
}
