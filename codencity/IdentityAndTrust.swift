//
//  IdentityAndTrust.swift
//  codencity
//
//  Created by tom Hastik on 18/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

struct IdentityAndTrust {

    var identityRef: SecIdentity
    var trust: SecTrust
    var certArray: AnyObject
}
