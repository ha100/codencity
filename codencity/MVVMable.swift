//
//  MVVMable.swift
//  codencity
//
//  Created by tom Hastik on 25/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import Foundation

protocol MVVMable: class {

    associatedtype ModelType

    var model: ModelType? { get set }
}
