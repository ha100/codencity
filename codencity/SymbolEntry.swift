//
//  SymbolEntry.swift
//  codencity
//
//  Created by tom Hastik on 12/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//
//  Credits: Matias Cudich https://github.com/mcudich/HeckelDiff

import Foundation

final class SymbolEntry {

    var oldCounter: Counter = .zero
    var newCounter: Counter = .zero
    var olno: Array<Int> = []

    var occursInBoth: Bool {
        return oldCounter != .zero && newCounter != .zero
    }
}
