//
//  DataRetrievable.swift
//  codencity
//
//  Created by tom Hastik on 11/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import Foundation

protocol DataRetrievable {
    func obtainData()
}
