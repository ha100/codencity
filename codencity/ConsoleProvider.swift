//
//  ConsoleProvider.swift
//  codencity
//
//  Created by tom Hastik on 29/06/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import Foundation

/// Analytics provider responsible for logging stuff into console
///
/// - Note: the >>> prefix is used for simplier console filtering
///
public final class ConsoleProvider: AnalyticsProvider {

    private let eventMapper = ConsoleEventMapper()

    private lazy var dateFormatter: DateFormatter = {

        DateFormatter().apply {

            if let timeZone = TimeZone(abbreviation: "UTC") {
                $0.timeZone = timeZone
            }

            $0.dateFormat = "HH:mm:ss.SSS"
        }
    }()

    public func report(event: AnalyticsEvent) {

        let name = eventMapper.eventName(for: event)

        guard name != "" else { return }

        print("\(dateFormatter.string(from: Date())) >>> \(name)")
    }
}
