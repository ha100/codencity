//
//  PacketList.swift
//  codencity
//
//  Created by tom Hastik on 15/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

enum PacketList {

    case getRepositories(user: String)
    case getUser(name: String)

    /// this will be trying to hit the mitmproxy in DEBUG mode
    ///
    /// - Tag: mockedUrl
    ///
    var baseUrl: String {

        #if MOCK
            // swiftlint:disable force_https
            return "http://\(settings.mitmIpAddress):8080"
            // swiftlint:enable force_https
        #else
            return "https://api.github.com"
        #endif

    }

    var url: String {

        switch self {

            case let .getRepositories(user): return "\(baseUrl)/users/\(user)/repos"

            case let .getUser(user): return "\(baseUrl)/users/\(user)"
        }
    }

    var request: Future<URLRequest> {

        guard let url = URL(string: self.url) else {

            analytics.report(event: .urlFailure(link: self.url, file: #file, function: #function, line: #line))
            return Promise(error: .urlCreationError)
        }

        var request = URLRequest(url: url)

        request.httpMethod = "GET"

        request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/vnd.github.v3+json", forHTTPHeaderField: "Accept")
        request.addValue("en-US", forHTTPHeaderField: "Accept-Language")
        request.addValue("gzip", forHTTPHeaderField: "Accept-Encoding")

        request.cachePolicy = .reloadIgnoringCacheData

        return Promise(value: request)
    }
}
