//
//  UIColor+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

/// allows color to be initialized from the hex value e.g. 0xd17934
///
/// ````
/// extension UIColor {
///
///   static let presidentialOrange = UIColor(0xd17934)
/// }
///
/// let convict = UILabel(frame: .zero)
/// convict.background = .presidentialOrange
/// ````
///
extension UIColor {

    static let lightBlue = UIColor(0x005395)

    /// UIColor convenience initializer allowing hex triplet init
    ///
    /// - Parameters:
    ///   - hex: Int value representing the color
    ///   - alpha: optional value defaults to 1
    ///
    /// ````
    /// extension UIColor {
    ///
    ///   static let presidentialOrange = UIColor(0xd17934)
    /// }
    ///
    /// let convict = UILabel(frame: .zero)
    /// convict.background = .presidentialOrange
    /// ````
    ///
    convenience init(_ hex: Int, alpha: CGFloat = 1) {

        let components = ( R: CGFloat((hex >> 16) & 0xff) / 255,
                           G: CGFloat((hex >> 08) & 0xff) / 255,
                           B: CGFloat((hex >> 00) & 0xff) / 255 )

        self.init(red: components.R, green: components.G, blue: components.B, alpha: alpha)
    }
}
