//
//  Diff.swift
//  codencity
//
//  Created by tom Hastik on 12/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//
//  Credits: Matias Cudich https://github.com/mcudich/HeckelDiff

import Foundation

/// Returns a diff, given an old and a new representation of a given collection (such as an `Array`).
/// The return value is a list of `Operation` values, each which instructs how to transform the old
/// collection into the new collection.
///
/// - parameter old: The old collection.
/// - parameter new: The new collection.
/// - returns: A list of `Operation` values, representing the diff.
/// - complexity: O(yeah)
/// - author: Matias Cudich
///
/// Based on http://dl.acm.org/citation.cfm?id=359467
///
/// And other similar implementations at:
/// - https://github.com/Instagram/IGListKit
/// - https://github.com/andre-alves/PHDiff
///
public func diff<T: Collection>(_ old: T, _ new: T) -> Array<Operation> where T.Iterator.Element: Hashable, T.Index == Int {

    var table: Dictionary<Int, SymbolEntry> = [:]
    var oldArray: Array<Entry> = []
    var newArray: Array<Entry> = []

    // Pass 1 comprises the following: (a) each line i of file N is read in sequence; (b) a symbol
    // table entry for each line i is created if it does not already exist; (c) NC for the line's
    // symbol table entry is incremented; and (d) NA [i] is set to point to the symbol table entry of
    // line i.
    for item in new {

        let entry = table[item.hashValue] ?? SymbolEntry()
        table[item.hashValue] = entry
        entry.newCounter.increment()
        newArray.append(.symbol(entry))
    }

    // Pass 2 is identical to pass 1 except that it acts on file O, array OA, and counter OC,
    // and OLNO for the symbol table entry is set to the line's number.
    for (index, item) in old.enumerated() {

        let entry = table[item.hashValue] ?? SymbolEntry()
        table[item.hashValue] = entry
        entry.oldCounter.increment()
        entry.olno.append(index)
        oldArray.append(.symbol(entry))
    }

    // In pass 3 we use observation 1 and process only those lines having NC = OC = 1. Since each
    // represents (we assume) the same unmodified line, for each we replace the symbol table pointers
    // in NA and OA by the number of the line in the other file. For example, if NA[i] corresponds to
    // such a line, we look NA[i] up in the symbol table and set NA[i] to OLNO and OA[OLNO] to i.
    // In pass 3 we also "find" unique virtual lines immediately before the first and immediately
    // after the last lines of the files.
    for (index, item) in newArray.enumerated() {

        if case let .symbol(entry) = item, entry.occursInBoth, !entry.olno.isEmpty {

            let oldIndex = entry.olno.removeFirst()
            newArray[index] = .index(oldIndex)
            oldArray[oldIndex] = .index(index)
        }
    }

    // In pass 4, we apply observation 2 and process each line in NA in ascending order: If NA[i]
    // points to OA[j] and NA[i + 1] and OA[j + 1] contain identical symbol table entry pointers, then
    // OA[j + 1] is set to line i + 1 and NA[i + 1] is set to line j + 1.
    var index = 1

    while index < newArray.count - 1 {

        if case let .index(oldIndex) = newArray[index], oldIndex + 1 < oldArray.count,
            case let .symbol(newEntry) = newArray[index + 1],
            case let .symbol(oldEntry) = oldArray[oldIndex + 1], newEntry === oldEntry {

            newArray[index + 1] = .index(oldIndex + 1)
            oldArray[oldIndex + 1] = .index(index + 1)
        }

        index += 1
    }

    // In pass 5, we also apply observation 2 and process each entry in descending order: if NA[i]
    // points to OA[j] and NA[i - 1] and OA[j - 1] contain identical symbol table pointers, then
    // NA[i - 1] is replaced by j - 1 and OA[j - 1] is replaced by i - 1.
    index = newArray.count - 1

    while index > 0 {

        if case let .index(oldIndex) = newArray[index], oldIndex - 1 >= 0,
            case let .symbol(newEntry) = newArray[index - 1],
            case let .symbol(oldEntry) = oldArray[oldIndex - 1], newEntry === oldEntry {

            newArray[index - 1] = .index(oldIndex - 1)
            oldArray[oldIndex - 1] = .index(index - 1)
        }

        index -= 1
    }

    var steps: Array<Operation> = []
    var deleteOffsets = Array(repeating: 0, count: old.count)
    var runningOffset = 0

    for (index, item) in oldArray.enumerated() {

        deleteOffsets[index] = runningOffset

        if case .symbol = item {

            steps.append(.delete(index))
            runningOffset += 1
        }
    }

    runningOffset = 0

    for (index, item) in newArray.enumerated() {

        switch item {

            case .symbol:

                steps.append(.insert(index))
                runningOffset += 1

            case let .index(oldIndex):

                // The object has changed, so it should be updated.
                if old[oldIndex] != new[index] {
                    steps.append(.update(index))
                }

                let deleteOffset = deleteOffsets[oldIndex]

                // The object is not at the expected position, so move it.
                if (oldIndex - deleteOffset + runningOffset) != index {
                    steps.append(.move(oldIndex, index))
                }
        }
    }

    return steps
}

/// Similar to to `diff`, except that this returns the same set of operations but in an order that
/// can be applied step-wise to transform the old array into the new one.
///
/// - parameter old: The old collection.
/// - parameter new: The new collection.
/// - returns: A list of `Operation` values, representing the diff.
/// - complexity: O(yeah)
/// - author: Matias Cudich
///
public func orderedDiff<T: Collection>(_ old: T, _ new: T) -> Array<Operation> where T.Iterator.Element: Hashable, T.Index == Int {

    let steps = diff(old, new)

    var insertions: Array<Operation> = []
    var updates: Array<Operation> = []
    var possibleDeletions: Array<Operation?> = Array(repeating: nil, count: old.count)

    let trackDeletion = { (fromIndex: Int, step: Operation) in

        if possibleDeletions[fromIndex] == nil {
            possibleDeletions[fromIndex] = step
        }
    }

    for step in steps {

        switch step {

            case .insert: insertions.append(step)

            case let .delete(fromIndex): trackDeletion(fromIndex, step)

            case let .move(fromIndex, toIndex):

                insertions.append(.insert(toIndex))
                trackDeletion(fromIndex, .delete(fromIndex))

            case .update: updates.append(step)
        }
    }

    let deletions = possibleDeletions.compactMap { $0 }.reversed()

    return deletions + insertions + updates
}
