//
//  MVVMController.swift
//  codencity
//
//  Created by tom Hastik on 25/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import UIKit

class MVVMController<ViewModelType>: UIViewController {

    var viewModel: ViewModelType?

    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {

        guard type(of: self) != MVVMController.self else {

            analytics.report(event: .subclassingFailure(object: "MVVMController"))
            fatalError()
        }

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {

        analytics.report(event: .deinit(object: "\(type(of: self))"))
    }
}
