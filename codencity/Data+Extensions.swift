//
//  Data+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import CommonCrypto
import Foundation

extension Data {

    /// parses the data to a meaningful `JsonRepresentable` object
    ///
    /// currently supported structures are:
    /// - Array
    /// - Dictionary
    ///
    ///````
    /// let string = "{ \"message\": \"Bad credentials\" }"
    /// let jsonFuture = string.decodeJSON()
    ///````
    ///
    /// - Returns: `Future` of the `JsonRepresentable` object or `jsonCreationError(_: String)`
    ///
    func decodeJSON() -> Future<JsonRepresentable> {

        return Promise { future in

            guard let json = try? JSONSerialization.jsonObject(with: self, options: []) as? JsonRepresentable else {

                let printable = String(data: self, encoding: .utf8)
                future.reject(with: .jsonCreationError(message: "failed to decode: \(String(describing: printable))"))
                return
            }

            future.resolve(with: json)
        }
    }

    func decodePlist() -> Result<Dictionary<String, AnyObject>, CodencityError> {

        if case let plist as Dictionary<String, AnyObject> = try? PropertyListSerialization.propertyList(from: self,
                                                                                                         options: .mutableContainers,
                                                                                                         format: nil) {
            return .success( plist )
        }

        let printable = String(data: self, encoding: .utf8)

        return .failure( .plistCreationError(message: "failed to decode: \(String(describing: printable))") )
    }

    /// generates sha256 hash from the data
    ///
    /// - Returns: sha256 hashed `Data`
    ///
    func sha256() -> Result<Data, CodencityError> {

        var hash = Array<UInt8>(repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))

        self.withUnsafeBytes {
            _ = CC_SHA256($0.baseAddress, CC_LONG(self.count), &hash)
        }

        return .success( Data(hash) )
    }

    /// parses the data into hexa `String`
    ///
    ///````
    /// let dataToString = "codencity".data(using: .utf8)?.hexString()
    /// print(dataToString)
    ///
    /// // 636f64656e63697479
    ///````
    ///
    /// - Returns: hex `String` representation of the `Data`
    ///
    func hexString() -> String {
        return self.map { String(format: "%02hhx", $0) }.joined()
    }
}
