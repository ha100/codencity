//
//  NetworkObservable.swift
//  codencity
//
//  Created by tom Hastik on 25/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

protocol NetworkPresentable {}

protocol NetworkObservable: class {

    func receivedNetworkData(data: NetworkPresentable?)
}
