//
//  AnalyticsProvider.swift
//  codencity
//
//  Created by tom Hastik on 03/05/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import Foundation

public protocol AnalyticsProvider {
    func report(event: AnalyticsEvent)
}
