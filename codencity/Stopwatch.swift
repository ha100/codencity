//
//  Stopwatch.swift
//  codencity
//
//  Created by tom Hastik on 09/06/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import Foundation

let stopwatch = Stopwatch.sharedInstance

/// helper singleton object allowing some work with time
///
/// - SeeAlso: Albert Einstein
///
final class Stopwatch: NSObject {

    // MARK: - Properties

    static let sharedInstance = Stopwatch()

    var startTime: CFAbsoluteTime?

    var time: String {

        let date = Date()
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.hour, .minute, .second], from: date)

        guard let hour = components.hour,
            let minutes = components.minute,
            let seconds = components.second else { return "💥" }

        return "\(String(describing: hour)):\(String(describing: minutes)):\(String(describing: seconds))"
    }

    // MARK: - Logic

    /// helper function allowing a task in the closure to be delayed by certain amount
    /// of time
    ///
    /// - Parameters:
    ///   - delay: `Double` number of seconds to wait before the task will be fired
    ///   - closure: actual work that needs to be done
    ///
    ///````
    /// delay(5) {
    ///
    ///     print("senor developer")
    ///
    /// }
    ///
    /// print("vamonos")
    ///
    /// // vamonos
    /// // senor developer
    ///````
    ///
    func delay(_ delay: Double, closure: @escaping () -> Void) {

        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }

    /// allows time measurement used to profile the app startup
    ///
    /// - Parameter event: MeasurementEvent describing the task for easier log search
    ///
    ///````
    /// stopwatch.startTime = CFAbsoluteTimeGetCurrent()
    /// stopwatch.measurement(.appLaunch)
    ///````
    ///
    /// - Note: the actual call logs the difference between the previously set `startTime` timestamp
    ///
    func measurement(_ event: MeasurementEvent) {

        guard let startTime = self.startTime else {

            analytics.report(event: .appMeasurementNoStartTime)
            return
        }

        let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
        analytics.report(event: .appMeasurement(time: timeElapsed, event: event))
    }

    /// allows time measurement of the given task
    ///
    /// - Parameters:
    ///   - title: MeasurementEvent describing the task for easier log search
    ///   - closure: task to be performed under the closer supervision
    ///
    ///````
    /// measure(.delay) {
    ///
    ///     delay(5) { print("space") }
    ///
    /// }
    ///
    /// // >>> ⏱ delay elapsed: 5 sec
    ///````
    ///
    func measure(_ event: MeasurementEvent, closure: (() -> Void) -> Void) {

        let startTime = CFAbsoluteTimeGetCurrent()

        closure {

            let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
            analytics.report(event: .appMeasurement(time: timeElapsed, event: event))
        }
    }
}
