//
//  String+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 19/05/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import Foundation

extension String {

    /// variable that allows further code simplifications
    ///
    /// used during localization
    ///
    /// this allows strings to be localized with
    ///````
    /// let title = "Title".localized
    ///````
    ///
    /// rather than with
    ///
    ///````
    /// let title = NSLocalizedString("Title", comment: "comment relevant for translation")
    ///````
    /// - Warning: use of this variable will render the xcode built in XLIFF export useless
    ///   and the localization team will not receive any comments relevant to the strings
    ///
    /// - Tag: stringLocalization
    ///
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
