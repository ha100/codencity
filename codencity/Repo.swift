//
//  Repo.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

struct Repo: Hashable {

    let language: Language
    let cloneUrl: URL
    let name: String
    let watchers: Int
}

func == (lhs: Repo, rhs: Repo) -> Bool {

    return lhs.language == rhs.language
        && lhs.cloneUrl == rhs.cloneUrl
        && lhs.name == rhs.name
        && lhs.watchers == rhs.watchers
}

extension Repo {

    init(_ object: Dictionary<String, AnyObject>) {

        self.language = object["language"] as? Language ?? .unknown

        self.cloneUrl = object["clone_url"]
            .map { String(describing: $0) }
            .flatMap { URL(string: $0) } ?? "https://www.ietf.org/rfc/rfc1738.txt"

        self.name = object["name"]
            .map { String(describing: $0) } ?? ""

        self.watchers = object["watchers"]
            .map { String(describing: $0) }
            .flatMap { Int($0) } ?? 0
    }
}

extension Repo: CustomStringConvertible {

    var description: String {

        var description = ""

        description += "REPO language: \(self.language)\n"
        description += "     cloneUrl: \(self.cloneUrl)\n"
        description += "     name: \(self.name)\n"
        description += "     watchers: \(self.watchers)"

        return description
    }
}
