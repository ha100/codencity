//
//  Array+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 24/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {

    /// remove elements by the instances in an array
    ///
    /// - Parameter element: generic element to be removed
    /// - Returns: index at which the elemenet has been located
    ///
    @discardableResult
    mutating func remove(element: Element) -> Index? {

        guard let index = firstIndex(of: element) else {

            print(">>> 💥 failed to remove element \(element) from array ")
            return nil
        }

        remove(at: index)

        return index
    }

    @discardableResult
    mutating func remove(elements: Array<Element>) -> Array<Index> {
        return elements.compactMap { remove(element: $0) }
    }
}
