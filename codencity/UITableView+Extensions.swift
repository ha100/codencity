//
//  UITableView+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

protocol CellCustomizable {

    /// encapsulated cell setup
    ///
    /// - Parameter object: object relevant for cell setup
    ///
    /// this allows cell setup to be moved from UITableView delegate method into the relevant cell
    ///````
    /// let item = self.itemAtIndexPath(tableView, indexPath: indexPath)
    /// let cell: CustomTableCell = tableView.dequeueReusableCell(for: indexPath)
    /// cell.configureCell(with: item)
    ///````
    ///
    /// rather than with
    ///
    ///````
    /// let item = self.itemAtIndexPath(tableView, indexPath: indexPath)
    /// let cell: CustomTableCell = tableView.dequeueReusableCell(for: indexPath)
    /// cell.titleLabel.text = item.title
    /// cell.subtitleLabel.text = item.subtitle
    ///````
    /// - Tag: cellSetup
    ///
    func configureCell<T>(with object: T)
}

protocol UIReusable: class {}

extension UIReusable where Self: UIView {

    /// variable that allows further code simplifications
    ///
    /// used during UITableViewCell registration and dequeuing
    ///
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: UIReusable {}

extension UITableView {

    /// simplified UITableviewCell registration
    ///
    /// - Parameter _: type of the generic cell to be registered
    ///
    /// this allows generic cell to be registered with one parameter
    ///````
    /// tableView.register(T.self)
    ///````
    ///
    /// rather than with
    ///
    ///````
    /// tableView.register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    ///````
    /// - Tag: cellRegistration
    ///
    func register<T: UITableViewCell>(_: T.Type) {
        self.register(T.classForCoder(), forCellReuseIdentifier: T.reuseIdentifier)
    }

    func register<T: UITableViewCell>(_ cells: [T.Type]) {
        cells.forEach { register($0.classForCoder(), forCellReuseIdentifier: T.reuseIdentifier) }
    }

    /// simplified UITableviewCell deque
    ///
    /// - Parameter indexPath: cell number to be dequed
    /// - Returns: generic cell
    ///
    /// this allows generic cell to be dequed with one parameter
    ///````
    /// let cell: T = tableView.dequeueReusableCell(for: indexPath)
    ///````
    ///
    /// rather than with force casted
    ///
    ///````
    /// let cell: T = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! T
    ///````
    /// - Tag: cellDeque
    ///
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {

        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {

            analytics.report(event: .cellDequeueFailure(identifier: T.reuseIdentifier))
            fatalError()
        }

        return cell
    }
}
