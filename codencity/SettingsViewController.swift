//
//  SettingsViewController.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class SettingsViewController: UIViewController {

    // MARK: - Properties

    lazy var attributes: Dictionary<NSAttributedString.Key, Any>? = {

        let fontName = "Helvetica"

        guard let font = UIFont(name: fontName, size: 12.0) else {

            analytics.report(event: .fontFailure(named: fontName, file: #file, function: #function, line: #line))
            return nil
        }

        return [ NSAttributedString.Key.font: font,
                 NSAttributedString.Key.foregroundColor: UIColor.white ]
    }()

    private lazy var versionLabel: UILabel = {

        UILabel().apply {

            $0.translatesAutoresizingMaskIntoConstraints = false
            $0.isAccessibilityElement = true
            $0.accessibilityLabel = L10n.SettingsController.VersionLabel.accessibilityLabel.localized
            $0.backgroundColor = .clear
            $0.attributedText = NSAttributedString(string: "\(Settings.appVersion)", attributes: self.attributes)
            $0.textAlignment = .center
            $0.numberOfLines = 0
        }
    }()

    // MARK: - UIView

    override func viewDidLoad() {

        super.viewDidLoad()

        self.navigationItem.title = L10n.SettingsController.NavigationItem.title.localized
        self.view.backgroundColor = .lightGray

        self.view.addSubview(self.versionLabel)
        self.setupConstraints()
    }

    func setupConstraints() {

        let constrz = Constrainer.version(view: self.versionLabel)
        NSLayoutConstraint.activate(constrz)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
