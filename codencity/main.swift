//
//  main.swift
//  codencity
//
//  Created by tom Hastik on 19/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

private var delegateClassName: String? = {
    NSClassFromString("XCTestCase") == nil ? NSStringFromClass(AppDelegate.self) : nil
}()

#if RELEASE
disable_attach()
#endif

stopwatch.startTime = CFAbsoluteTimeGetCurrent()

UIApplicationMain(CommandLine.argc, CommandLine.unsafeArgv, nil, delegateClassName)
