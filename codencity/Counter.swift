//
//  Counter.swift
//  codencity
//
//  Created by tom Hastik on 12/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//
//  Credits: Matias Cudich https://github.com/mcudich/HeckelDiff

import Foundation

enum Counter {

    case zero
    case one
    case many

    mutating func increment() {

        switch self {

            case .zero: self = .one

            case .one: self = .many

            case .many: break
        }
    }
}
