//
//  JsonRepresentable.swift
//  codencity
//
//  Created by tom Hastik on 15/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

protocol JsonRepresentable {}

extension NSDictionary: JsonRepresentable {}

extension NSArray: JsonRepresentable {}

func == (lhs: Dictionary<String, AnyObject>, rhs: Dictionary<String, AnyObject>) -> Bool {
    return NSDictionary(dictionary: lhs).isEqual(to: rhs)
}
