//
//  DetailCoordinator.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

public final class DetailCoordinator: NSObject {

    // MARK: - Properties

    var root: UINavigationController
    var viewController: UIViewController?
    var model: Repo

    // MARK: - Init

    init(root: UINavigationController, repo: Repo) {

        self.root = root
        self.model = repo
    }
}

// MARK: - Coordinating

extension DetailCoordinator: NavigationCoordinating {

    func createViewController() -> UIViewController {
        return DetailViewController(self.model)
    }

    func configure(_ viewController: UIViewController) {

        viewController.view.backgroundColor = .lightGray
    }
}
