//
//  Operation.swift
//  codencity
//
//  Created by tom Hastik on 12/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//
//  Credits: Matias Cudich https://github.com/mcudich/HeckelDiff

import Foundation

/// Used to represent the operation to perform on the source array. Indices indicate the position at
/// which to perform the given operation.
///
/// - insert: Insert a new value at the given index.
/// - delete: Delete a value at the given index.
/// - move:   Move a value from the given origin index, to the given destination index.
/// - update: Update the value at the given index.
public enum Operation: Equatable {

    case insert(Int)
    case delete(Int)
    case move(Int, Int)
    case update(Int)

    public static func == (lhs: Operation, rhs: Operation) -> Bool {

        switch (lhs, rhs) {

            case let (.insert(leftValue), .insert(rightValue)),
                 let (.delete(leftValue), .delete(rightValue)),
                 let (.update(leftValue), .update(rightValue)): return leftValue == rightValue

            case let (.move(leftValue), .move(rightValue)): return leftValue == rightValue

            default: return false
        }
    }
}
