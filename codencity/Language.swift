//
//  Language.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

public enum Language: String {

    case actionScript   = "ActionScript"
    case appleScript    = "AppleScript"
    case clang          = "C"
    case cPlusPlus      = "C++"
    case golang         = "Go"
    case objectiveC     = "Objective-C"
    case php            = "PHP"
    case python         = "Python"
    case shell          = "Shell"
    case swift          = "Swift"
    case unknown        = "Unknown"
}
