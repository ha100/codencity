//
//  Protector.swift
//  codencity
//
//  Created by tom Hastik on 18/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import CommonCrypto
import Foundation

/// object conforming to `URLSessionDelegate` responsible for certificate pinning
/// and two way certificate validation
///
/// - Tag: certificatePinning
///
final class Protector: NSObject {

    // MARK: - Auxiliary Functions

    func publicKeys(for trust: SecTrust) -> Array<SecKey> {

        return (0 ..< SecTrustGetCertificateCount(trust))
            .compactMap { SecTrustGetCertificateAtIndex(trust, $0) }
            .compactMap { publicKey(for: $0).value }
    }

    func publicKey(for certificate: SecCertificate) -> Result<SecKey, CodencityError> {

        var optionalTrust: SecTrust?
        let trustCreationStatus = SecTrustCreateWithCertificates(certificate, SecPolicyCreateBasicX509(), &optionalTrust)

        guard trustCreationStatus == errSecSuccess,
            let trust = optionalTrust else { return .failure( .trustCreateError ) }

        guard let publicKey = SecTrustCopyPublicKey(trust) else { return .failure( .trustPubKeyError ) }

        return .success( publicKey )
    }

    func publicKeyRefToHash(_ publicKeyRef: SecKey) -> Result<String, CodencityError> {

        return publicKeyRefToData(publicKeyRef)
            .flatMap { $0.sha256() }
            .map { $0.base64EncodedString() }
    }

    func publicKeyRefToData(_ publicKeyRef: SecKey) -> Result<Data, CodencityError> {

        let keychainTag = "X509_KEY"
        var publicKeyData: CFTypeRef?
        var putResult = noErr
        var delResult = noErr

        let putKeyParams = [ kSecClass as String: kSecClassKey,
                             kSecAttrApplicationTag as String: keychainTag,
                             kSecValueRef as String: publicKeyRef,
                             kSecReturnData as String: true ] as CFDictionary

        let delKeyParams = [ kSecClass as String: kSecClassKey,
                             kSecAttrApplicationTag as String: keychainTag,
                             kSecReturnData as String: true ] as CFDictionary

        putResult = SecItemAdd(putKeyParams, &publicKeyData)
        delResult = SecItemDelete(delKeyParams)

        guard putResult == errSecSuccess,
            delResult == errSecSuccess,
            let result = publicKeyData as? Data else { return .failure( .publicKeyRefDataError ) }

        return .success( result )
    }

    func extractIdentity() -> Result<IdentityAndTrust, CodencityError> {

        var identityAndTrust: IdentityAndTrust

        guard let path = Bundle.main.url(forResource: settings.certNameClient,
                                         withExtension: "p12") else { return .failure( .certPathError ) }

        do {
            guard let PKCS12Data = try? Data(contentsOf: path) else { return .failure( .certDataError ) }

            let key = kSecImportExportPassphrase as String
            let options: Dictionary<String, CFString> = [key: settings.certPasswordClient]

            var items: CFArray?

            guard case let securityError = SecPKCS12Import(PKCS12Data as CFData, options as CFDictionary, &items),
                securityError == errSecSuccess && CFArrayGetCount(items) > 0 else { return .failure( .certBlobError ) }

            guard let certItems: CFArray = items else { return .failure( .certCFArrayError ) }

            let certItemsArray: Array = certItems as Array
            let dict: AnyObject? = certItemsArray.first

            guard let certEntry = dict as? Dictionary<String, AnyObject> else { return .failure( .certEntryError ) }
            guard let identityPointer: AnyObject = certEntry["identity"] else { return .failure( .certIdentityError ) }

            // swiftlint:disable force_cast
            let secIdentityRef = identityPointer as! SecIdentity
            // swiftlint:enable force_cast

            guard let trustPointer = certEntry["trust"] else { return .failure( .certTrustError ) }

            // swiftlint:disable force_cast
            let trustRef = trustPointer as! SecTrust
            // swiftlint:enable force_cast

            guard let chainPointer = certEntry["chain"] else { return .failure( .certChainError ) }

            identityAndTrust = IdentityAndTrust(identityRef: secIdentityRef, trust: trustRef, certArray: chainPointer)

            return .success(identityAndTrust)
        }
    }
}

extension Protector: URLSessionDelegate {

    func urlSession(_ session: URLSession,
                    didReceive challenge: URLAuthenticationChallenge,
                    completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {

        var disposition: URLSession.AuthChallengeDisposition = .cancelAuthenticationChallenge
        var credential: URLCredential?

        switch challenge.protectionSpace.authenticationMethod {

            case NSURLAuthenticationMethodServerTrust:

                var secresult = SecTrustResultType.invalid

                guard let serverTrust = challenge.protectionSpace.serverTrust,
                    SecTrustEvaluate(serverTrust, &secresult) == errSecSuccess,
                    SecTrustGetCertificateCount(serverTrust) > 0 else { return }

                let policy = SecPolicyCreateSSL(true, challenge.protectionSpace.host as CFString)
                SecTrustSetPolicies(serverTrust, policy)

                let serverTrustIsValid = publicKeys(for: serverTrust)
                    .map(publicKeyRefToHash)
                    .first(where: { $0.value == settings.serverTrustHashes[challenge.protectionSpace.host] })
                    .map { _ in true } ?? false

                guard serverTrustIsValid else {

                    let setting = settings.serverTrustHashes[challenge.protectionSpace.host]
                    let hash = publicKeys(for: serverTrust)
                            .map(publicKeyRefToHash)
                            .compactMap { $0.value }

                    let errorMsg = """
                    >>> 💥 🔒 certPinning \(challenge.protectionSpace.host)
                    >>> ℹ️ local \"\(setting ?? "")\" != server \(hash.description)
                    >>> ℹ️ PUT LEAF HASH FROM PREVIOUSLY DUMPED SERVER HASH ARRAY INTO Settings.plist serverTrustHashes
                    """
                    print(errorMsg)
                    break
                }

                disposition = .useCredential
                credential = URLCredential(trust: serverTrust)

            case NSURLAuthenticationMethodClientCertificate:

                disposition = .useCredential
                credential = self.extractIdentity().map { URLCredential(identity: $0.identityRef,
                                                                        certificates: $0.certArray as? [AnyObject],
                                                                        persistence: .forSession) }.value

            default: break
        }

        completionHandler(disposition, credential)
    }
}
