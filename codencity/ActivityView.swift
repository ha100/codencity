//
//  ActivityView.swift
//  codencity
//
//  Created by tom Hastik on 13/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import UIKit

final class ActivityView: UIView {

    // MARK: - Properties

    var circleRadius: CGFloat { return self.frame.size.width / 2 }
    let duration = 2.0

    lazy var circlePathLayer: CAShapeLayer = {

        CAShapeLayer().apply {

            $0.frame = self.bounds
            $0.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            $0.lineWidth = 7
            $0.fillColor = UIColor.clear.cgColor
            $0.strokeColor = UIColor.lightBlue.cgColor
        }
    }()

    lazy var circleLayer: CAShapeLayer = {

        CAShapeLayer().apply {

            $0.frame = self.bounds
            $0.lineWidth = 11
            $0.fillColor = UIColor.clear.cgColor
            $0.strokeColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        }
    }()

    lazy var groupAnimation: CAAnimationGroup = {

        let inAnimation = CAKeyframeAnimation().apply {

            $0.keyPath = "strokeEnd"
            $0.duration = self.duration
            $0.values = [0, 0.7, 1]
        }

        let outAnimation = CAKeyframeAnimation().apply {

            $0.keyPath = "strokeStart"
            $0.beginTime = self.duration * 0.5
            $0.duration = self.duration * 0.5
            $0.values = [0, 0.6, 1]
        }

        return CAAnimationGroup().apply {

            $0.animations = [ inAnimation, outAnimation ]
            $0.duration = self.duration
            $0.repeatCount = Float.infinity
            $0.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            $0.fillMode = .forwards
            $0.isRemovedOnCompletion = false
        }
    }()

    lazy var rotationAnimation: CABasicAnimation = {

        CABasicAnimation().apply {

            $0.keyPath = "transform.rotation.z"
            $0.fromValue = 0
            $0.toValue = Double.pi * 2
            $0.duration = self.duration
            $0.repeatCount = Float.infinity
            $0.isRemovedOnCompletion = false
        }
    }()

    // MARK: - Init

    convenience init() {

        self.init(frame: .zero)
        configure()
    }

    func configure() {

        layer.addSublayer(circleLayer)
        layer.addSublayer(circlePathLayer)

        backgroundColor = .clear

        self.rotate()
    }

    func rotate() {

        self.circlePathLayer.add(self.rotationAnimation, forKey: nil)
        self.circlePathLayer.add(self.groupAnimation, forKey: nil)
    }

    override func layoutSubviews() {

        super.layoutSubviews()

        circleLayer.frame = bounds
        circleLayer.path = UIBezierPath(ovalIn: self.bounds.insetBy(dx: 10, dy: 10)).cgPath

        circlePathLayer.frame = bounds
        circlePathLayer.path = UIBezierPath(ovalIn: self.bounds.insetBy(dx: 10, dy: 10)).cgPath
        circlePathLayer.lineCap = .round
    }
}
