//
//  Utils.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

func getFile(path: String = #file) -> String {

    let parts = path.components(separatedBy: "/")
    return parts[parts.count - 1]
}

func log(_ text: String, file: String, functionName: String = #function, line: Int = #line) {
    print("💥 \(stopwatch.time) \(file) > \(functionName) [line \(line)] \(text)")
}
