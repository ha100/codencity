//
//  LogLevel.swift
//  codencity
//
//  Created by tom Hastik on 30/06/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import Foundation

/// Logging level for the app
///
///  - none: disable logging
///  - trace: log all logs
///  - debug: log debug logs or higher
///  - info: log info logs or higher
///  - warn: log warning logs or higher
///  - error: log error logs or higher
///  - fatal: log fatal logs only
///
public enum LogLevel: Int {

    case none = 0
    case trace = 100
    case debug = 200
    case info = 300
    case warn = 400
    case error = 500
    case fatal = 600

    var string: String {

        switch self {

            case .none: return "NONE"

            case .trace: return "TRACE"

            case .debug: return "DEBUG"

            case .info: return "INFO "

            case .warn: return "WARN "

            case .error: return "ERROR"

            case .fatal: return "FATAL"
        }
    }
}
