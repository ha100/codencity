//
//  URL+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 20/05/2018.
//  Copyright © 2018 ha100. All rights reserved.
//
//  https://twitter.com/johnsundell/status/886876157479616513

import Foundation

extension URL: ExpressibleByStringLiteral {

    public init(stringLiteral value: StaticString) {
        self = URL(string: "\(value)").require(hint: "Invalid URL string literal: \(value)")
    }
}

public extension URL {

    init(extendedGraphemeClusterLiteral value: StaticString) {
        self.init(stringLiteral: value)
    }

    init(unicodeScalarLiteral value: StaticString) {
        self.init(stringLiteral: value)
    }
}
