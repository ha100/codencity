//
//  Result.swift
//  codencity
//
//  Created by tom Hastik on 15/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//
//  Credits: https://github.com/antitypical/Result

import Foundation

/// enum type representing either `success` with a value or `failure` with an error
///
/// - success: in this case the valid outcome of the operation will be
///            caried as value `T`
/// - failure: in this case the invalid outcome of the operation will be
///            represented by error `E`
///
enum Result<T, E: Error> {

    case success(T)
    case failure(E)
}

extension Result {

    /// map transform on the `Result` type
    ///
    /// - Parameter function: to be aplied to the value `T` of successful case
    /// - Returns: new Result by transforming the value using function, or
    ///            re-wrapping the error of the failed case
    ///
    @discardableResult
    func map<U>(_ function: (T) -> U ) -> Result<U, E> {
        return flatMap { .success( function( $0 ) ) }
    }

    /// flatMap transform on the result type
    ///
    /// - Parameter function: to be aplied to the value `T` of successful case
    /// - Returns: result of applying funnction to value, or re-wrapping
    ///            the error of the failed case
    ///
    func flatMap<U>(_ function: (T) -> Result<U, E> ) -> Result<U, E> {

        switch self {

            case let .success(value): return function(value)

            case let .failure(error): return .failure(error)
        }
    }

    /// helper function that interlinks two `Result` types
    /// and avoids switch nesting
    ///
    /// ````
    /// extension String {
    ///
    ///     func usernameValid() -> Result<String, CSError> {
    ///     ⋮
    ///     }
    ///
    ///     func passwordValid() -> Result<String, CSError> {
    ///     ⋮
    ///     }
    /// }
    ///
    /// let validationResult = "johnSnow@theNorth.gt"
    ///                             .usernameValid()
    ///                             .interlinked(
    ///                                 "secret_wannaKillJoffreySoFuckingBaad_password".passwordValid()
    ///                              )
    /// ````
    ///
    /// - Parameter function: @autoclosure of the second `Result`
    /// - Returns: Error if either one of the two linked Results failed or
    ///            tuple containing both values
    ///
    func interlinked<NextValue>(_ function: @autoclosure () -> Result<NextValue, E>) -> Result<(T, NextValue), E> {

        return self.flatMap { firstValue in
            function().map { secondValue in (firstValue, secondValue) }
        }
    }
}

extension Result {

    /// Returns the value if self represents a success, `nil` otherwise.
    ///
    var value: T? {

        switch self {

            case let .success(value): return value

            case .failure: return nil
        }
    }
    /// Returns the error if self represents a failure, `nil` otherwise.
    ///
    var error: E? {

        switch self {

            case .success: return nil

            case let .failure(error): return error
        }
    }
}

extension Result where T: Equatable, E: Equatable {

    /// function allowing equality comparison of two `Result` types
    ///
    /// - Parameters:
    ///   - left: left result
    ///   - right: right result
    /// - Returns: `Bool` outcome of the comparison operation
    ///
    static func == (left: Result<T, E>, right: Result<T, E>) -> Bool {

        if let left = left.value, let right = right.value { return left == right }

        else if let left = left.error, let right = right.error { return left == right }

        return false
    }

    /// function allowing inequality comparison of two `Result` types
    ///
    /// - Parameters:
    ///   - left: left `Result`
    ///   - right: right `Result`
    /// - Returns: `Bool` outcome of the comparison operation
    ///
    static func != (left: Result<T, E>, right: Result<T, E>) -> Bool {
        return !(left == right)
    }
}

extension Result: CustomStringConvertible {

    /// cu100m textual representation for better debugging
    ///
    public var description: String {

        switch self {

            case let .success(value): return ".success(\(value))"

            case let .failure(error): return ".failure(\(error))"
        }
    }
}

extension Result: CustomDebugStringConvertible {

    /// cu100m textual representation for better debugging
    ///
    public var debugDescription: String {
        return description
    }
}
