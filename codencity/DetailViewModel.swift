//
//  DetailViewModel.swift
//  codencity
//
//  Created by tom Hastik on 25/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import Foundation

final class DetailViewModel: MVVMable {

    // MARK: - Properties

    var model: Repo?

    var name: String {
        return self.model?.name ?? ""
    }

    weak var networkDelegate: NetworkObservable?

    // MARK: - Init

    init(delegate: NetworkObservable, repo: Repo) {

        self.networkDelegate = delegate
        self.model = repo
        self.obtainData()
    }
}

extension DetailViewModel: DataRetrievable {

    func obtainData() {

        API
            .getUser(named: settings.githubUser)
            .observe { user in
                self.networkDelegate?.receivedNetworkData(data: user)
            }
    }
}
