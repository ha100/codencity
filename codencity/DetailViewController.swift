//
//  DetailViewController.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class DetailViewController: MVVMController<DetailViewModel> {

    // MARK: - Init

    convenience init(_ withObject: Repo) {

        self.init(nibName: nil, bundle: nil)
        self.viewModel = DetailViewModel(delegate: self, repo: withObject)
    }

    // MARK: - UIView

    override func viewDidLoad() {

        super.viewDidLoad()
        self.navigationItem.title = self.viewModel?.name
        self.view.backgroundColor = .white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension DetailViewController: NetworkObservable {

    func receivedNetworkData(data: NetworkPresentable?) {

        guard let user = data as? User else {

            let event: AnalyticsEvent = .unexpectedObjectFailure(expecting: "\(User.self)",
                                                                 got: "\(type(of: data))",
                                                                 file: #file,
                                                                 function: #function,
                                                                 line: #line)
            analytics.report(event: event)
            return
        }

        print(">>> followers: \(user.followers)")
    }
}
