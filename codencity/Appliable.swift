//
//  Appliable.swift
//  codencity
//
//  Created by tom Hastik on 24/12/2018.
//  Copyright © 2018 ha100. All rights reserved.
//

import Foundation

/// allows entity preporties to be setup within a closure with
///
///````
///lazy var settingsButton: UIButton = {
///
///    UIButton(type: .custom).apply {
///
///        $0.frame = CGRect(x: 0, y: 0, width: 55, height: 55)
///        $0.isAccessibilityElement = true
///        $0.accessibilityLabel = "settings"
///    }
///}()
///````
///
/// rather than with
///````
///lazy var settingsButton: UIButton = {
///
///    let button = UIButton(type: .custom)
///
///    button.frame = CGRect(x: 0, y: 0, width: 55, height: 55)
///    button.isAccessibilityElement = true
///    button.accessibilityLabel = "settings"
///
///    return button
///}()
///````
/// - Author: Tatsuya Tanaka @tattn
/// - SeeAlso: [My favorite Swift extensions](https://dev.to/tattn/my-favorite--swift-extensions-8g7)
/// - Tag: lazySetup
///
protocol Appliable {}

extension Appliable {

    /// helper function allowing setup with kotlin-like scope function
    ///
    /// - Parameter closure: containing the object setup
    /// - Returns: object after setup
    ///
    ///````
    ///lazy var settingsButton: UIButton = {
    ///
    ///    UIButton(type: .custom).apply {
    ///
    ///        $0.frame = CGRect(x: 0, y: 0, width: 55, height: 55)
    ///        $0.isAccessibilityElement = true
    ///        $0.accessibilityLabel = "settings"
    ///    }
    ///}()
    ///````
    ///
    /// - Author: Tatsuya Tanaka @tattn
    /// - SeeAlso: [My favorite Swift extensions](https://dev.to/tattn/my-favorite--swift-extensions-8g7)
    ///
    @discardableResult
    func apply(closure: (Self) -> Void) -> Self {

        closure(self)
        return self
    }
}

/// Kotlin-like scope function
protocol Runnable {}

extension Runnable {

    @discardableResult
    func run<T>(closure: (Self) -> T) -> T {
        return closure(self)
    }
}

extension NSObject: Appliable {}
extension NSObject: Runnable {}
