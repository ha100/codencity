//
//  Future.swift
//  codencity
//
//  Created by tom Hastik on 19/04/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

#if os(Linux)
import Dispatch
#endif
import Foundation

// swiftlint:disable file_length

/// `Future` type encapsulating a deferred computation, that can be `observed` later on in the future.
///
/// ````
/// func obtainNetworkedData() -> Future<Data> {
/// ⋮
/// }
/// ````
///
/// - SeeAlso: - [Under the hood of Futures & Promises in Swift]
///              (https://www.swiftbysundell.com/posts/under-the-hood-of-futures-and-promises-in-swift)
///            - [Implementing Promises in Swift]
///              (https://felginep.github.io/2019-01-06/implementing-promises-in-swift)
///            - [A Promise library for Swift, based partially on Javascript's A+ spec]
///              (https://github.com/khanlou/Promise)
///            - [Lightweight full-featured Promises, Async & Await Library in Swift]
///              (https://github.com/malcommac/Hydra)
///            - [Promises Library for Swift with Async/Await]
///              (https://github.com/freshOS/then)
///            - [A Swift based Future/Promises Library for IOS and OS X.]
///              (https://github.com/FutureKit/FutureKit)

///
open class Future<T: Any> {

    public enum State<T> {

        case pending(progress: Float)
        case resolved(T)
        case rejected(CodencityError)

        /// Resolved `value` associated with the state. `nil` if the state is not `resolved`.
        var value: T? {

            guard case let .resolved(value) = self else { return nil }
            return value
        }

        /// Error associated with the state. `nil` if the state is not `rejected`.
        var error: CodencityError? {

            guard case let .rejected(error) = self else { return nil }
            return error
        }

        /// Return `true` if the promise is in `pending` state, `false` otherwise.
        var isPending: Bool {

            guard case .pending = self else { return false }
            return true
        }

        var isRejected: Bool {

            guard case .rejected = self else { return false }
            return true
        }
    }

    // MARK: - Properties

    internal lazy var callbacks: Array<(T) -> Void> = []
    internal lazy var errorCallbacks: Array<(CodencityError) -> Void> = []

    /// `State` of the `Promise`. Initially a promise has a `pending` state.
    ///
    internal var state: State<T> = .pending(progress: 0)

    /// promise debug identifier
    ///
    public var name = ""
    var reporter: AnalyticsProvider?

    // MARK: - Init

    deinit {

        self.callbacks.removeAll()
        self.errorCallbacks.removeAll()

        reporter?.report(event: .deinit(object: "\(type(of: self)) - \(self.name)"))
    }

    // MARK: - Public Functions

    /// helper function that resolves the previously created and returned `Promise`
    /// with a valid value that has been returned by some async call
    ///
    /// ````
    /// func obtainNetworkedData(urlString: String) -> Future<Data> {
    ///
    ///     let promise = Promise<Data>
    ///     ⋮
    ///     // after some time data comes from network
    ///     promise.resolve(with: Data(bytes: [0x42, 0x42, 0x42]))
    ///     ⋮
    ///     return promise
    /// }
    /// ````
    ///
    /// - Parameter value: success value for `Promise` to be resolved with.
    ///
    public func resolve(with value: T) {

        updateState(to: .resolved(value))
    }

    /// helper function that rejects the previously created and returned `Promise`
    /// with an error that has been returned by some async call
    ///
    /// ````
    /// func obtainNetworkedData(urlString: String) -> Future<Data> {
    ///
    ///     let promise = Promise<Data>
    ///     ⋮
    ///     // network returns 404 error
    ///     promise.reject(error: .notFoundError)
    ///     ⋮
    ///     return promise
    /// }
    /// ````
    ///
    /// - Parameter error: error value for promise to be rejected with
    ///
    public func reject(with error: CodencityError) {

        updateState(to: .rejected(error))
    }

    // MARK: - Private Functions

    /// function that can update the `State` in case the `Promise` is still in a `pending` state
    ///
    /// - Warning: once a `Promise` is `resolved` or `rejected`, the promise's value MUST not be changed
    ///
    /// - Parameter newState: will probably be set to `resolved`
    ///
    private func updateState(to newState: State<T>) {

        guard case .pending = self.state else { return }

        self.state = newState
        self.triggerCallbacks(for: self.state)
    }

    /// method responsible for letting the observers know about the state change
    ///
    /// - Parameter state: current `Promise` state
    ///
    // swiftlint:disable strict_fileprivate
    fileprivate func triggerCallbacks(for state: State<T>) {

        switch state {

            case let .resolved(value):

                self.callbacks.forEach { $0(value) }
                self.callbacks.removeAll()

            case let .rejected(error):

                self.errorCallbacks.forEach { $0(error) }
                self.errorCallbacks.removeAll()

            default: break
        }
    }
    // swiftlint:enable strict_fileprivate
}

public extension Future where T == Void {

    /// helper function that resolves the previously created and returned `Promise`
    /// with a nil value
    ///
    /// ````
    /// func obtainNetworkedData(urlString: String) -> Future<Void> {
    ///
    ///     let promise = Promise<Void>
    ///     ⋮
    ///     // after some time
    ///     promise.resolve()
    ///     ⋮
    ///     return promise
    /// }
    /// ````
    ///
    func resolve() {
        updateState(to: .resolved(()))
    }
}

extension Future: CustomStringConvertible {

    /// description of the state of this promise.
    ///
    public var description: String {

        return self.name
    }
}

extension Future: CustomDebugStringConvertible {

    /// description of the state of this promise.
    ///
    public var debugDescription: String {
        return self.description
    }
}

// MARK: - Promise

/// `Promise` type as a `Future` type subclass encapsulating a deferred computation,
/// that can be `observed` later on in the future. it is used from inside of the
/// function that will return `Future`. returned type will be either success with
/// a value `T` or failure with an error `E`
///
/// ````
/// func obtainNetworkedData() -> Future<Data> {
///
///     let promise = Promise<Data>
///     ⋮
///     // some async network call resolving/rejecting later on
///     ⋮
///     return promise
/// }
/// OR
/// func obtainNetworkedData() -> Future<Data> {
///
///     return Promise<Data> { future in
///     ⋮
///         // some async network call resolving/rejecting later on
///         future.resolve(with: Data())
///     ⋮
///     }
/// }
/// ````
///
/// - Note: we create a `Promise` and return from the function right away with
///         with the value comming later depending on the type of the async operation
///         this value will have to be observed later
///
/// - Warning: do not use previously created `Promise` inside the next chained block
///            since this would create retain cycle
///
/// - SeeAlso: - [Under the hood of Futures & Promises in Swift]
///              (https://www.swiftbysundell.com/posts/under-the-hood-of-futures-and-promises-in-swift)
///            - [Implementing Promises in Swift]
///              (https://felginep.github.io/2019-01-06/implementing-promises-in-swift)
///            - [A Promise library for Swift, based partially on Javascript's A+ spec]
///              (https://github.com/khanlou/Promise)
///            - [Lightweight full-featured Promises, Async & Await Library in Swift]
///              (https://github.com/malcommac/Hydra)
///            - [Promises Library for Swift with Async/Await]
///              (https://github.com/freshOS/then)
///            - [A Swift based Future/Promises Library for IOS and OS X.]
///              (https://github.com/FutureKit/FutureKit)
///
public final class Promise<T>: Future<T> {

    // MARK: - Init

    /// convenience initializer that removes the burden of initializing and
    /// specifying the `Promise` type right after it has been declared in
    /// function signature return type
    ///
    ///````
    /// func getData(id: Int) -> Future<Data> {
    ///
    ///     return Promise { future in
    ///         ⋮
    ///         future.resolve(with: data)
    ///         ⋮
    ///     }
    /// }
    ///````
    /// vs
    ///````
    /// func getData(id: Int) -> Future<Data> {
    ///
    ///    let promise = Promise<Data>()
    ///    ⋮
    ///    promise.resolve(with: data)
    ///    ⋮
    ///    return future
    /// }
    ///````
    ///
    public init(reporter: AnalyticsProvider = AnalyticsReporter.instance,
                file: String = #file,
                function: String = #function,
                _ callback: @escaping (Future<T>) -> Void) {

        super.init()
        self.name = "\(String(describing: file.components(separatedBy: "/").last)): \(function)"
        self.reporter = reporter

        callback(self)
    }

    /// initilaizer allowing creation of the `Promise` with a value
    /// that is already known at the time of it's creation
    ///
    /// - Note: this is needed in case we are unable to resolve the `Promise`.
    ///         for example because of the `guard`'s early exit from the function
    ///         statement since we would not reach the end of the function where
    ///         we usually return the `Promise` type
    ///
    /// ````
    /// func obtainNetworkedData(urlString: String) -> Future<Data> {
    ///
    ///     if let cachedImageData = DTO(objectFor: urlString) {
    ///         return Promise(value: cachedImageData)
    ///     }
    ///
    ///     let promise = Promise<Data>
    ///     ⋮
    ///     // original return value
    ///     return promise
    /// }
    /// ````
    ///
    /// - Parameter value: Generic type to be returned
    ///
    public init(_ reporter: AnalyticsProvider = AnalyticsReporter.instance,
                _ file: String = #file,
                _ function: String = #function,
                value: T? = nil) {

        super.init()
        self.name = "\(String(describing: file.components(separatedBy: "/").last)): \(function)"
        self.reporter = reporter

        self.state = value.map { .resolved($0) } ?? .pending(progress: 0)
    }

    /// initilaizer allowing creation of the `Promise` with `CodencityError`
    /// that is already known at the time of it's creation
    ///
    /// - Note: this is generally needed in case we are 100% sure we will be unable
    ///         to resolve the `Promise`. we can't reject as usual, because of the
    ///         `guard`'s early exit (1) from the function statement since we would
    ///         not reach the end (2) of the function where we usually return the
    ///         `Promise`. compiler keeps usually bitchin about missing return value
    ///         anyway
    ///
    /// ````
    /// func obtainNetworkedData(urlString: String) -> Future<Data> {
    ///
    ///     guard let url = URL(string: urlString) else {
    ///         return Promise(error: .urlCreationError)    <----- 1)
    ///     }
    ///
    ///     let promise = Promise<Data>
    ///     ⋮
    ///     // original return value that will never be reached
    ///     return promise                                  <----- 2)
    /// }
    /// ````
    ///
    /// - Parameter error: `CodencityError` to be returned
    ///
    public init(_ reporter: AnalyticsProvider = AnalyticsReporter.instance,
                _ file: String = #file,
                _ function: String = #function,
                error: CodencityError) {

        super.init()
        self.name = "\(String(describing: file.components(separatedBy: "/").last)): \(function)"
        self.reporter = reporter

        self.state = .rejected(error)
    }
}

// MARK: - Transformations Functions

public extension Future {

    /// Waits for all the `Promises` of the same type to fulfill, then `resolve`s
    /// itself with the `Array` of values. if any of them fails, this chain is
    /// doomed
    ///
    /// - Parameter promises: `Futures` as a supeerclass of `Promise` to be resolved
    /// - Returns: return values from all of the `Promises`
    ///
    static func all(_ promises: Future<T>...) -> Future<[T]> {

        return Promise<[T]> { future in

            for promise in promises {

                promise.observe { value in

                    if !promises.contains(where: { $0.state.isRejected || $0.state.isPending }) {

                        future.resolve(with: promises.compactMap { $0.state.value })
                    }
                }
                .catch { error in

                    future.reject(with: error)
                }
            }
        }
    }

    // MARK: - Always

    /// adds a closure containing optional `T` and `Error` types which
    /// will be always called upon the `Future` completion. would be usually
    /// used for logging or smthn.
    ///
    /// - Parameter closure: that will always execute at the end
    /// - Returns: `Future` value
    ///
    /// ````
    /// return Promise { future in
    /// ⋮
    ///     future.resolve(with: data)
    /// ⋮
    /// }
    /// .observe { value in
    /// ⋮
    /// }
    /// .catch { error in
    /// ⋮
    /// }
    /// .always { value in
    /// ⋮
    ///     analytics.report(event: .appResignActive)
    /// }
    /// ````
    ///
    @discardableResult
    func always(_ closure: @escaping (T?, CodencityError?) -> Void) -> Future<T> {

        self.callbacks.append { value in
            closure(value, nil)
        }

        self.errorCallbacks.append { error in
            closure(nil, error)
        }

        return self
    }

    // MARK: - Catch

    /// the provided closure executes when any `Promise` in the chain fails
    ///
    /// - Note: Rejecting a `Promise` cascades: rejecting all subsequent promises (unless
    ///         recover is invoked) thus you will typically place your catch at the end
    ///         of a chain
    ///
    /// - Parameter closure: that will be executed in the case of non-recoverable error
    /// - Returns: `Future` value
    ///
    /// ````
    /// return Promise { future in
    /// ⋮
    ///     future.resolve(with: data)
    /// ⋮
    /// }
    /// .observe { value in
    /// ⋮
    /// }
    /// .catch { error in
    /// ⋮
    /// }
    /// ````
    ///
    @discardableResult
    func `catch`(_ closure: @escaping (CodencityError) -> Void) -> Future<T> {

        self.errorCallbacks.append(closure)
        self.triggerCallbacks(for: self.state)

        return self
    }

    // MARK: - Chained

    /// helper function that allows chaining of the multiple `Future` types
    /// multiple other frameworks use the name `then`
    ///
    /// ````
    /// func obtainNetworkedData(urlString: String) -> Future<Data> {
    /// ⋮
    /// }
    ///
    /// func decrypt(crypted data: Data) -> Future<Data> {
    /// ⋮
    /// }
    ///
    /// func unzip(decrypted data: Data) -> Future<Data> {
    /// ⋮
    /// }
    ///
    /// let unzippedData = obtainNetworkedData(urlString: "http://…")
    ///                         .chained(decrypt)
    ///                         .chained(unzip)
    /// ````
    ///
    /// - Note: the unzippedData value will be of type Future<Data> and still
    ///         needs to be handled with `observed` and `catch` closure to
    ///         determine if it is success or failure
    ///
    /// - Parameter closure: that will take a `Future` and transform it
    /// - Returns: new `Future`
    ///
    func chained<NextValue>(_ closure: @escaping (T) -> Future<NextValue>) -> Future<NextValue> {

        return Promise<NextValue> { future in

            self.observe { value in

                closure(value).observe { result in

                    future.resolve(with: result)
                }
                .catch { error in

                    future.reject(with: error)
                }

            }
            .catch { error in

                future.reject(with: error)
            }
        }
    }

    // MARK: - Delay

    /// Delay executon of a `Promise` chain part by number of seconds
    ///
    /// - Parameter delay: `TimeInterval` in seconds
    /// - Returns: `Future` value
    ///
    /// ````
    /// return Promise { future in
    /// ⋮
    ///     future.resolve(with: data)
    /// ⋮
    /// }
    /// .delay(5)
    /// .observe { value in
    /// ⋮
    /// }
    /// .catch { error in
    /// ⋮
    /// }
    /// ````
    ///
    func delay(_ delay: TimeInterval) -> Future<Void> {

        return Promise { future in

            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                future.resolve(with: ())
            }
        }
    }

    /// Delay executon of a whole `Promise` chain by number of seconds
    ///
    /// - Note: this functionality is used in `timeout` function of this
    ///         `Promise` implementation
    ///
    /// - Parameter delay: `TimeInterval` in seconds
    /// - Returns: `Future` value
    ///
    /// ````
    /// Promise
    ///     .delay(5)
    ///     .observe { future in
    ///     ⋮
    ///     }
    /// ````
    ///
    static func delay(_ delay: TimeInterval) -> Future<Void> {

        return Promise { future in

            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                future.resolve(with: ())
            }
        }
    }

    /// helper function that allows to observe the value of the successfuly `resolved` `Future`
    ///
    /// - Note: `observe` can be called anytime, before or after the `Promise` has a value.
    ///          If the `Promise` is not resolved yet and we call `observe` on it, the value
    ///          is not available, so we have to store the `closure` parameter.
    ///
    /// ````
    /// func obtainNetworkedData(urlString: String) -> Future<Data> {
    /// ⋮
    /// }
    ///
    /// func decrypt(crypted data: Data) -> Future<Data> {
    /// ⋮
    /// }
    ///
    /// func unzip(decrypted data: Data) -> Future<Data> {
    /// ⋮
    /// }
    ///
    /// obtainNetworkedData(urlString: "http://…")
    ///     .chained(decrypt)
    ///     .chained(unzip)
    ///     .observe { value in
    ///     ⋮
    ///         print(">>> successfuly obtained: \(value)")
    ///     ⋮
    ///     }
    ///     .catch { error in
    ///     ⋮
    ///         print(">>> the process failed with: \(error)")
    ///     ⋮
    ///     }
    /// ````
    ///
    /// - Parameter closure: closure to be implemmented from outside to allow
    ///                      work with the returned value/error
    /// - Returns: `Future` value
    ///
    @discardableResult
    func observe(_ closure: @escaping (T) -> Void) -> Future<T> {

        self.callbacks.append(closure)
        self.triggerCallbacks(for: self.state)

        return self
    }

    // MARK: - Race

    /// returns the outcome of the fastest returning `Promise`
    /// If the first fails, it fails. If the first resolves, it resolves.
    ///
    /// - Parameter promises: `Promises` to execute
    /// - Returns: `Future` value
    ///
    static func race(_ promises: Future<T>...) -> Future<T> {

        return Promise { future in

            promises.forEach { promise in

                promise.observe { value in

                    future.resolve(with: value)
                }
                .catch { error in

                    future.reject(with: error)
                }
            }
        }
    }

    // MARK: - Recover

    /// Will try to recover from a `rejected` `Promise`
    ///
    /// - Parameter closure: execute the callback and see if we can go further in chain
    /// - Returns: `Future` value
    ///
    func recover(_ closure: @escaping (CodencityError) -> Future<T>) -> Future<T> {

        return Promise { future in

            self.observe { value in

                future.resolve(with: value)
            }
            .catch { error in

                closure(error)
                    .observe { value in

                        future.resolve(with: value)
                    }
                    .catch { error in

                        future.reject(with: error)
                    }
            }
        }
    }

    // MARK: - Retry

    /// Will try to recover from failure by trying multiple times
    ///
    /// - Parameters:
    ///   - count: `Int` that will decrease till 0 is reached
    ///   - delay: `Int` seconds between each retry
    ///   - closure: callback to be invoked
    /// - Returns: `Future` value
    ///
    func retry(count: Int,
               delay: TimeInterval,
               closure: @escaping () -> Future<T>) -> Future<T> {

        // swiftlint:disable empty_count
        guard count > 0 else {
            return Promise(error: .promiseRetryLimitReachedError)
        }
        // swiftlint:enable empty_count

        return Promise { future in

            closure()
                .recover { _ in

                    self.delay(delay)
                        .chained { _ in

                            self.retry(count: count - 1,
                                       delay: delay,
                                       closure: closure)
                        }
                }
                .observe {

                    future.resolve(with: $0)
                }
                .catch {

                    future.reject(with: $0)
                }
        }
    }

    // MARK: - Timeout

    /// /// `Promise` that will be rejected after a delay.
    ///
    /// - Parameter countdown: `TimeInterval` in seconds
    /// - Returns: `Future` value
    ///
    func timeout(_ countdown: TimeInterval) -> Future<T> {

        let timer = Promise<T> { future in

            Promise<T>
                .delay(countdown)
                .observe { _ in

                    future.reject(with: .promiseExecutionTimedOut)
                }
        }

        return Promise.race(timer, self)
    }

    // MARK: - Transform

    /// `map` helper function that allows transformation of the `Future` value
    /// to a new type
    ///
    /// ````
    /// func obtainNetworkedData(urlString: String) -> Future<Data> {
    /// ⋮
    /// }
    ///
    /// let stringFuture = obtainNetworkedData(urlString: "http://…")
    ///                         .transform { String(data: $0, encoding: .utf8) ?? "" }
    /// ````
    ///
    /// - Parameter closure: that will take a `Future` and transform it
    /// - Returns: new `Future` with transformed value
    ///
    func transform<NewValue>(_ closure: @escaping (T) -> NewValue) -> Future<NewValue> {

        return Promise { future in

            self.observe { value in

                future.resolve(with: closure(value))
            }
            .catch { error in

                future.reject(with: error)
            }
        }
    }

    // MARK: - Validate

    /// operator that allows you to validate the result of a `Promise` with a predicate
    /// closure where you can return `true` or `false`.
    ///
    /// - Parameter closure: responsible for `Promise` validation
    /// - Returns: previously resolved `Promise` after validation
    ///
    func validate(_ closure: @escaping (T) -> Bool) -> Future<T> {

        return Promise { future in

            self.observe { value in

                if closure(value) == false {

                    future.reject(with: .promiseValidationError)

                } else {

                    future.resolve(with: value)
                }
            }
        }
    }

    // MARK: - zip

    /// operator that allows you to validate the result of two `Promise`s with
    /// heterogenious types.
    ///
    /// - Parameters:
    ///   - a: first `Promise` to be resolved
    ///   - b: second `Promise` to be resolved
    /// - Returns: outcome of the `Promise`s
    ///
    static func zip<T, U>(_ a: Future<T>, _ b: Future<U>) -> Future<(T, U)> {

        return Promise { future in

            a.observe { aValue in

                if !b.state.isRejected || !b.state.isPending,
                    let bValue = b.state.value {

                    future.resolve(with: (aValue, bValue))
                }
            }
            .catch { error in

                future.reject(with: error)
            }

            b.observe { bValue in

                if !a.state.isRejected || !a.state.isPending,
                    let aValue = a.state.value {

                    future.resolve(with: (aValue, bValue))
                }
            }
            .catch { error in

                future.reject(with: error)
            }
        }
    }
}
