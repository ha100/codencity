//
//  API.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

typealias JSON = Dictionary<String, AnyObject>

public final class API: Networking {

    /// obtains repositories for a given user
    ///
    /// - Parameters:
    ///   - session: override this with mock for unit testing, otherwise
    ///              the default `URLSession` object will be used
    ///   - user: `String` representation of the `User` to get the `Repo` for
    /// - Returns: `Array` of `Repo`
    ///
    static func getRepos(_ session: URLSession = URLSession(configuration: .default,
                                                            delegate: Protector(),
                                                            delegateQueue: nil),
                         for user: String) -> Future<Array<Repo>> {

        return Promise<Array<Repo>> { future in

            send(session, packet: .getRepositories(user: user))
                .transform { response -> Array<Repo> in

                    guard let jsonResponse = response as? Array<JSON> else {
                        return []
                    }

                    return jsonResponse.compactMap { Repo($0) }
                }
                .observe { jsonArray in

                    future.resolve(with: jsonArray)
                }
                .catch { error in

                    print(">>> 💥 \(#file) \(#function) \(#line) \(error)")
                    future.resolve(with: [])
                }
        }
    }

    /// obtains detail information for a given user
    ///
    /// - Parameters:
    ///   - session: override this with mock for unit testing, otherwise
    ///              the default `URLSession` object will be used
    ///   - named: `String` representation of the `User` to get the `Repo` for
    /// - Returns: `User`
    ///
    static func getUser(_ session: URLSession = URLSession(configuration: .default,
                                                           delegate: Protector(),
                                                           delegateQueue: nil),
                        named: String) -> Future<User> {

        return Promise<User> { future in

            send(session, packet: .getUser(name: named))
                .transform { response -> User? in

                    guard let jsonResponse = response as? Dictionary<String, AnyObject> else {
                        return nil
                    }

                    return User(jsonResponse)
                }
                .observe { userOptional in

                    guard let user = userOptional else {

                        future.reject(with: .apiUserParsingError)
                        return
                    }

                    future.resolve(with: user)
                }
                .catch { error in

                    print(">>> 💥 \(#file) \(#function) \(#line) \(error)")
                    future.reject(with: error)
                }
        }
    }
}
