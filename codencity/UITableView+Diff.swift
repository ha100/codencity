//
//  UITableView+Diff.swift
//  codencity
//
//  Created by tom Hastik on 12/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//
//  Credits: Matias Cudich https://github.com/mcudich/HeckelDiff

import UIKit

public extension UITableView {

    /// Applies a batch update to the receiver, efficiently
    ///
    /// - parameter update:           updates to be animated
    /// - parameter animation:        The animation type.
    /// - parameter reloadUpdated:    Whether or not updated cells should be reloaded (default: true)
    func applyDiff(_ update: ListUpdate,
                   withAnimation animation: UITableView.RowAnimation,
                   reloadUpdated: Bool = true) {

        beginUpdates()

        deleteRows(at: update.deletions, with: animation)
        insertRows(at: update.insertions, with: animation)

        for move in update.moves {
            moveRow(at: move.from, to: move.to)
        }

        endUpdates()

        // reloadItems is done separately as the update indexes returne by diff() are in respect to the
        // "after" state, but the collectionView.reloadItems() call wants the "before" indexPaths.
        if reloadUpdated && update.updates.isEmpty {

            beginUpdates()
            reloadRows(at: update.updates, with: animation)
            endUpdates()
        }
    }
}
