//
//  Settings.swift
//  codencity
//
//  Created by tom Hastik on 18/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

let settings = Settings.sharedInstance

final class Settings: NSObject {

    static let sharedInstance = Settings()

    var serverTrustHashes: Dictionary<String, String>
    var githubUser: String
    var certNameClient: String
    var certPasswordClient: CFString
    var mitmIpAddress: String

    static var appVersion: String {

        guard let buildString = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion"),
            let versionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") else { return "undefined" }

        return "\(versionString).\(buildString)"
    }

    convenience override init() {

        guard let data = Bundle.main.path(forResource: "Settings", ofType: "plist")
            .flatMap({ URL(string: "file://\($0)") })
            .flatMap({ try? Data(contentsOf: $0) })
            .flatMap({ $0.decodePlist().value }) else { print(">>> 💥 settings plist"); self.init([:]); return }

        self.init(data)
    }

    init(_ object: Dictionary<String, AnyObject>) {

        self.serverTrustHashes = object["serverTrustHashes"] as? Dictionary<String, String> ?? [:]
        self.githubUser = object["githubUser"].map { String(describing: $0) } ?? ""
        self.certNameClient = object["certNameClient"].map { String(describing: $0) } ?? ""
        self.certPasswordClient = object["certPasswordClient"].map { String(describing: $0) as CFString } ?? "" as CFString
        self.mitmIpAddress = object["mitmIpAddress"].map { String(describing: $0) } ?? "0.0.0.0"
    }
}

extension Settings {

    override var debugDescription: String {

        var result = ">>> \(self.className) [\n"
        result += ">>>     githubUser: \(githubUser)\n"
        result += ">>>     certNameClient: \(certNameClient)\n"
        result += ">>>     certPasswordClient: \(certPasswordClient)\n"
        result += ">>>\n"
        result += ">>>     serverTrustHashes: \(serverTrustHashes)\n"
        result += ">>> ]"

        return result
    }
}
