//
//  CustomTableCell.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

final class CustomTableCell: UITableViewCell {

    // MARK: - Properties

    var cellObject: Repo?
    let fontSize: CGFloat = 18.0

    lazy var grayTextFontAttributes: Dictionary<NSAttributedString.Key, Any> = {

        let fontName = "Helvetica"

        guard let font = UIFont(name: fontName, size: self.fontSize) else {

            analytics.report(event: .fontFailure(named: fontName, file: #file, function: #function, line: #line))
            return [:]
        }

        return [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: self.color]
    }()

    lazy var color: UIColor = {
        UIColor.black
    }()

    lazy var shapeLayer: CAShapeLayer = {

        CAShapeLayer().apply {

            $0.lineWidth = 0.5
            $0.strokeColor = UIColor.black.cgColor
            $0.fillColor = UIColor.clear.cgColor
        }
    }()

    let padding: CGFloat = 5
    lazy var paddedHeight: CGFloat = { self.bounds.height - (2 * padding) }()

    lazy var finalRect: CGRect = {
        CGRect(x: paddedHeight * 0.25, y: padding, width: paddedHeight, height: paddedHeight)
    }()

    lazy var startShape: CGPath = {
        UIBezierPath(roundedRect: finalRect.insetBy(dx: 20, dy: 20), cornerRadius: 3).cgPath
    }()

    lazy var endShape: CGPath = {
        UIBezierPath(roundedRect: finalRect, cornerRadius: 7).cgPath
    }()

    let animationDuration: TimeInterval = 0.73

    // MARK: - Init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {

        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.isAccessibilityElement = true
        layer.addSublayer(shapeLayer)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Drawing

    override func draw(_ rect: CGRect) {

        color.withAlphaComponent(0.7).setStroke()

        self.animate()

        if let text = cellObject?.name {

            let halfHeight = rect.height * 0.5

            let textRect = CGRect(x: rect.height * 1.5, y: halfHeight - (fontSize * 0.5),
                                  width: rect.width - rect.height * 1.5, height: halfHeight)

            text.draw(in: textRect, withAttributes: grayTextFontAttributes)
        }
    }

    func animate() {

        shapeLayer.path = startShape

        let animation = CABasicAnimation(keyPath: #keyPath(CAShapeLayer.path))
        animation.timingFunction = CAMediaTimingFunction(controlPoints: 1.00, 0.0, 0.35, 1.0)
        animation.duration = animationDuration
        animation.toValue = endShape
        animation.fillMode = .forwards
        animation.isRemovedOnCompletion = false

        shapeLayer.add(animation, forKey: #keyPath(CAShapeLayer.path))
    }
}

extension CustomTableCell: CellCustomizable {

    func configureCell<T>(with object: T) {

        guard let item = object as? Repo else {

            analytics.report(event: .unexpectedObjectFailure(expecting: "\(Repo.self)", got: "\(T.self)", file: #file, function: #function, line: #line))
            return
        }

        self.cellObject = item
        self.accessibilityLabel = item.name
    }
}
