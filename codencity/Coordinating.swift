//
//  Coordinating.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

protocol Coordinating: class {

    associatedtype RootViewController: UIViewController
    associatedtype ViewController: UIViewController

    var root: RootViewController { get }
    var viewController: ViewController? { get set }

    func createViewController() -> ViewController
    func configure(_ viewController: ViewController)
    func show(_ viewController: ViewController)
    func dismiss()
}

extension Coordinating {

    func configure(_ viewController: UIViewController) {}

    func show(_ viewController: UIViewController) {
        root.present(viewController, animated: true, completion: nil)
    }

    func dismiss() {
        root.dismiss(animated: true, completion: nil)
    }

    func start() {

        let controller = createViewController()
        viewController = controller
        configure(controller)
        show(controller)
    }

    func stop() {

        dismiss()
        viewController = nil
    }
}
