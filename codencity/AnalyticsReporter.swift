//
//  AnalyticsReporter.swift
//  codencity
//
//  Created by tom Hastik on 03/05/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import Foundation

let analytics = AnalyticsReporter.instance

public final class AnalyticsReporter {

    public static let instance = AnalyticsReporter()

    private let providers: Array<AnalyticsProvider>

    private init(_ providers: Array<AnalyticsProvider> = [ ConsoleProvider() ] ) {
        self.providers = providers
    }

    public func report(event: AnalyticsEvent) {

        providers.forEach { $0.report(event: event) }
    }
}

extension AnalyticsReporter: AnalyticsProvider {}
