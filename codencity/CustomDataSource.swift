//
//  CustomDataSource.swift
//  codencity
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import UIKit

/// generic object conforming to `UITableViewDataSource` used to populate the `UITableViews`
///
/// ````
///  lazy var tableView: UITableView = {
///
///      UITableView(frame: .zero).apply {
///          ⋮
///          $0.register(CustomTableCell.self)
///          $0.dataSource = CustomDataSource<Repo>(CustomTableCell.reuseIdentifier)
///          ⋮
///      }
///  }()
/// ````
/// - Tag: dataSource
///
final class CustomDataSource<T>: NSObject, UITableViewDataSource {

    // MARK: - Properties

    var contents: Array<T>
    var cellIdentifier: String

    // MARK: - Init

    /// CustomDataSource initializer
    ///
    /// - Parameter identifier: String used during cell dequeue
    ///
    /// ````
    ///  lazy var tableView: UITableView = {
    ///
    ///      UITableView(frame: .zero).apply {
    ///          ⋮
    ///          $0.register(CustomTableCell.self)
    ///          $0.dataSource = CustomDataSource<Repo>(CustomTableCell.reuseIdentifier)
    ///          ⋮
    ///      }
    ///  }()
    /// ````
    /// - Warning: identifier needs to be registered for `UITableView` use
    ///
    init(_ identifier: String) {

        cellIdentifier = identifier
        contents = Array<T>()
    }

    // MARK: - UITableViewDataSource

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let item = self.itemAtIndexPath(tableView, indexPath: indexPath)

        let cell: CustomTableCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configureCell(with: item)
        cell.selectionStyle = .none
        cell.setNeedsDisplay()

        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {

            contents.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }

    // MARK: - Auxiliary methods

    func itemAtIndexPath(_ forTable: UITableView, indexPath: IndexPath ) -> T {
        return self[indexPath.row]
    }
}

// MARK: - Sequence

extension CustomDataSource: Sequence {

    func makeIterator() -> AnyIterator<T> {

        var index = 0

        return AnyIterator {

            guard index < self.contents.count else {

                print(">>> 💥 \(#file) \(#function) \(#line) iterator bounds failure")
                return nil
            }

            index += 1

            return self.contents[index]
        }
    }
}

// MARK: - Collection

extension CustomDataSource: Collection {

    typealias Index = Int

    var startIndex: Index {
        return contents.startIndex
    }

    var endIndex: Index {
        return contents.endIndex
    }

    subscript(position: Index) -> T {
        return contents[position]
    }

    func index(before index: Index) -> Index {
        return index - 1
    }

    func index(after index: Index) -> Index {
        return index + 1
    }
}
