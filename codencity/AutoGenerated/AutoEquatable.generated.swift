// Generated using Sourcery 0.16.1 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


/// handles optional enum comparison
///
/// - Parameters:
///   - lhs: left optional value
///   - rhs: right optional value
///   - compare: comparison function
/// - Returns: bool of the outcome
///
/// - Warning: do not edit - this file is autogenerated with every build
///                          from a stencil file by sourcery framework
///
private func compareOptionals<T>(lhs: T?,
                                 rhs: T?,
                                 compare: (_ lhs: T, _ rhs: T) -> Bool) -> Bool {

    switch (lhs, rhs) {

        case let (lValue?, rValue?): return compare(lValue, rValue)

        case (nil, nil): return true

        default: return false
    }
}

/// handles array comparison
///
/// - Parameters:
///   - lhs: left array value
///   - rhs: right array value
///   - compare: comparison function
/// - Returns: bool of the outcome
///
/// - Warning: do not edit - this file is autogenerated with every build
///                          from a stencil file by sourcery framework
///
private func compareArrays<T>(lhs: [T],
                              rhs: [T],
                              compare: (_ lhs: T, _ rhs: T) -> Bool) -> Bool {

    guard lhs.count == rhs.count else { return false }

    for (idx, lhsItem) in lhs.enumerated() {

        guard compare(lhsItem, rhs[idx]) else { return false }
    }

    return true
}

// MARK: - AutoEquatable

extension CodencityError: Equatable {}

/// function handling comparison of the enum cases
///
/// - Parameters:
///   - lhs: left enum value
///   - rhs: right enum value
/// - Returns: bool outcome of the operation
///
/// - Warning: do not edit - this file is autogenerated with every build
///                          from a stencil file by sourcery framework
///
public func == (lhs: CodencityError, rhs: CodencityError) -> Bool {

    switch (lhs, rhs) {

        case (.unknownError(let lhs), .unknownError(let rhs)):

            return lhs == rhs

        case (.urlCreationError, .urlCreationError):

            return true

        case (.networkError(let lhs), .networkError(let rhs)):

            if lhs.message != rhs.message

            { return false }
            if lhs.code != rhs.code

            { return false }

            return true

        case (.networkEmptyData, .networkEmptyData):

            return true

        case (.jsonCreationError(let lhs), .jsonCreationError(let rhs)):

            return lhs == rhs

        case (.plistCreationError(let lhs), .plistCreationError(let rhs)):

            return lhs == rhs

        case (.trustCreateError, .trustCreateError):

            return true

        case (.trustPubKeyError, .trustPubKeyError):

            return true

        case (.publicKeyRefDataError, .publicKeyRefDataError):

            return true

        case (.certPathError, .certPathError):

            return true

        case (.certDataError, .certDataError):

            return true

        case (.certBlobError, .certBlobError):

            return true

        case (.certCFArrayError, .certCFArrayError):

            return true

        case (.certEntryError, .certEntryError):

            return true

        case (.certIdentityError, .certIdentityError):

            return true

        case (.certChainError, .certChainError):

            return true

        case (.certTrustError, .certTrustError):

            return true

        case (.apiUserParsingError, .apiUserParsingError):

            return true

        case (.promiseValidationError, .promiseValidationError):

            return true

        case (.promiseRetryLimitReachedError, .promiseRetryLimitReachedError):

            return true

        case (.promiseExecutionTimedOut, .promiseExecutionTimedOut):

            return true

        default: return false
    }
}

