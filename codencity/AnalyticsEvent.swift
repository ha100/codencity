//
//  AnalyticsEvent.swift
//  codencity
//
//  Created by tom Hastik on 03/05/2019.
//  Copyright © 2019 ha100. All rights reserved.
//

import UIKit

public enum AnalyticsEvent: Equatable {

    // MARK: - appDelegate

    case appBecomeActive
    case appEnterBackground
    case appMeasurement(time: Double, event: MeasurementEvent)
    case appMeasurementNoStartTime
    case appResignActive
    case appTerminate

    case cellDequeueFailure(identifier: String)

    case controllerFailure(file: String, function: String, line: Int)

    case `deinit`(object: String)

    case fontFailure(named: String, file: String, function: String, line: Int)

    case subclassingFailure(object: String)

    case unexpectedObjectFailure(expecting: String, got: String, file: String, function: String, line: Int)
    case urlFailure(link: String, file: String, function: String, line: Int)
}

public extension AnalyticsEvent {

    var parameters: Dictionary<Parameter, String> {

        var dict: Dictionary<Parameter, String> = [:]

        if let udid = UIDevice.current.identifierForVendor?.uuid {

            dict.updateValue("\(udid)", forKey: .serialNumber)
        }

        switch self {

            default: return dict
        }
    }

    enum Parameter: String, Equatable {

        case serialNumber
    }
}
