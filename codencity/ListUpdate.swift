//
//  ListUpdate.swift
//  codencity
//
//  Created by tom Hastik on 12/01/2019.
//  Copyright © 2019 ha100. All rights reserved.
//
//  Credits: Matias Cudich https://github.com/mcudich/HeckelDiff

import Foundation

public struct ListUpdate {

    public var deletions: Array<IndexPath> = []
    public var insertions: Array<IndexPath> = []
    public var updates: Array<IndexPath> = []
    public var moves: Array<(from: IndexPath, to: IndexPath)> = []

    public init(_ result: Array<Operation>, _ section: Int = 0) {

        for step in result {

            switch step {

                case let .delete(index): deletions.append(IndexPath(row: index, section: section))

                case let .insert(index): insertions.append(IndexPath(row: index, section: section))

                case let .update(index): updates.append(IndexPath(row: index, section: section))

                case let .move(fromIndex, toIndex): moves.append((from: IndexPath(row: fromIndex, section: section),
                                                                  to: IndexPath(row: toIndex, section: section)))
            }
        }
    }
}
