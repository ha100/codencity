//
//  CodencityError.swift
//  codencity
//
//  Created by tom Hastik on 15/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

/// project wide `Error` type
///
public enum CodencityError: Error, AutoEquatable, AutoCaseName {

    case unknownError(msg: String)
    case urlCreationError

    case networkError(message: String, code: Int)
    case networkEmptyData

    case jsonCreationError(message: String)
    case plistCreationError(message: String)

    case trustCreateError
    case trustPubKeyError
    case publicKeyRefDataError

    case certPathError
    case certDataError
    case certBlobError
    case certCFArrayError
    case certEntryError
    case certIdentityError
    case certChainError
    case certTrustError

    case apiUserParsingError

    // MARK: - Promises

    case promiseValidationError
    case promiseRetryLimitReachedError
    case promiseExecutionTimedOut

    /// default initializer for unhandled errors
    ///
    /// - Parameter msg: context description to be linked to the error object
    ///
    init(msg: String) {
        self = .unknownError(msg: msg)
    }
}
