//
//  URL+Extensions.swift
//  codencity
//
//  Created by tom Hastik on 20/05/2018.
//  Copyright © 2018 ha100. All rights reserved.
//
//  https://twitter.com/johnsundell/status/886876157479616513

import Foundation

extension Optional {

    func require(hint hintExpression: @autoclosure () -> String? = nil,
                 file: StaticString = #file,
                 line: UInt = #line) -> Wrapped {

        guard let unwrapped = self else {

            var message = "Required value was nil in \(file), at line \(line)"

            if let hint = hintExpression() {
                message.append(". Debugging hint: \(hint)")
            }

            #if !os(Linux)
            let exception = NSException(
                name: .invalidArgumentException,
                reason: message,
                userInfo: nil
            )

            exception.raise()
            #endif

            preconditionFailure(message)
        }

        return unwrapped
    }
}
