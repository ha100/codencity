//
//  Networking.swift
//  codencity
//
//  Created by tom Hastik on 15/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

/// allows object conforming to this protocol to communicate via network
/// by calling the `send` function
///
///````
/// send(session, packet: .getUser(name: named))
///     .observe { result in
///
///         switch result {
///
///             case let .success(response as JSON):
///
///                completion( User(response) )
///
///             case let .failure(error):
///
///                print(">>> 💥 \(#file) \(#function) \(#line) \(error)")
///                fallthrough
///
///             default: completion(nil)
///         }
///  }
///````
///
/// - Tag: networking
///
protocol Networking {}

extension Networking {

    /// helper function responsible for the network heavy lifting
    ///
    /// - Parameters:
    ///   - session: `URLSession` to be used for communication - override this for unitTesting
    ///   - packet: `PacketList` object
    /// - Returns: `Future` of `JsonRepresentable` object or `CodencityError`
    ///
    static func send(_ session: URLSession,
                     packet: PacketList) -> Future<JsonRepresentable> {

        return packet.request.chained { request-> Future<Data> in

            Promise { future in

            session.dataTask(with: request) { data, response, error in

                guard let httpResponse = response as? HTTPURLResponse else {

                    future.reject(with: CodencityError(msg: error?.localizedDescription ?? "emptyResponse"))
                    return
                }

                guard error == nil else {

                    future.reject(with: .networkError(message: error?.localizedDescription ?? "N/A", code: httpResponse.statusCode))
                    return
                }

                guard 200 ... 299 ~= httpResponse.statusCode else {

                    future.reject(with: .networkError(message: error?.localizedDescription ?? "N/A", code: httpResponse.statusCode))
                    return
                }

                guard let httpData = data else { future.reject(with: .networkEmptyData); return }

                    future.resolve(with: httpData)

                }.resume()
            }
        }
        .chained { $0.decodeJSON() }
    }
}
