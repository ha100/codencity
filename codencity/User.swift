//
//  User.swift
//  codencity
//
//  Created by tom Hastik on 17/11/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import Foundation

struct User {

    let login: String
    let name: String
    let id: Int
    let followers: Int
}

extension User: NetworkPresentable {}

extension User {

    init(_ object: Dictionary<String, AnyObject>) {

        self.login = object["login"]
            .map { String(describing: $0) } ?? ""

        self.name = object["name"]
            .map { String(describing: $0) } ?? ""

        self.id = object["id"]
            .map { String(describing: $0) }
            .flatMap { Int($0) } ?? 0

        self.followers = object["followers"]
            .map { String(describing: $0) }
            .flatMap { Int($0) } ?? 0

    }
}
