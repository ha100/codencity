//
//  CodencityUITests.swift
//  codencityUITests
//
//  Created by tom Hastik on 05/03/2017.
//  Copyright © 2017 ha100. All rights reserved.
//

import XCTest

class CodencityUITests: XCTestCase {

    override func setUp() {
        super.setUp()

        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens
        // for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for
        // your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDetailControllerTitleIsSet() {

        let title = "3D-in-SDL"

        XCUIApplication()
            .tables["repos"]
            .cells[title]
            .children(matching: .other)
            .element.tap()

        XCTAssert(XCUIApplication().navigationBars[title].exists)
    }

    func testSettingsControllerTitleSet() {

        let title = "settings"

        XCUIApplication()
            .navigationBars
            .buttons[title].tap()

        XCTAssert(XCUIApplication().navigationBars[title].exists)
    }
}
